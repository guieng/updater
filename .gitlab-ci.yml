# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>


# Common parameters
# ===========================================================================
default:
  tags:
    - dntd
    - docker

stages:
  - lint
  - prepare
  - test
  - package
  - branch
  - complete


## Shared jobs
# ===========================================================================
.docker:
  image: "registry.hub.docker.com/library/docker:stable"
  before_script:
    - docker --version
    - |
      echo "${CI_JOB_TOKEN}" | \
      docker login \
             --password-stdin "${CI_REGISTRY}" \
             --username "gitlab-ci-token"

.git:
  image: registry.hub.docker.com/gitscm/git:latest
  before_script:
    - git config --local user.name "${GITLAB_USER_NAME}"
    - git config --local user.email "${GITLAB_USER_EMAIL}"
    - git config --local credential.helper "cache --timeout=2147483647"
    - printf "url=${CI_PROJECT_URL}\nusername=${CI_BOT_USER}\npassword=${CI_PERSONAL_TOKEN}\n\n" | git credential approve
    - git remote set-url --push origin "https://${CI_BOT_USER}@${CI_REPOSITORY_URL#*@}"
  after_script:
    - git credential-cache exit
  variables:
    GIT_DEPTH: "0"


# Linting
# ===========================================================================
.linting:
  stage: lint

shellscript_linting:
  extends: .linting
  image: "registry.hub.docker.com/koalaman/shellcheck-alpine:stable"
  before_script:
    - shellcheck --version
  script:
    - shellcheck --color=always --format=tty --shell=sh --external-sources **/*.sh

dockerfile_linting:
  extends: .linting
  image: "registry.hub.docker.com/hadolint/hadolint:latest-debian"
  before_script:
    - hadolint --version
  script:
    - hadolint --format tty **Dockerfile*


# Build environment setup
# ===========================================================================
prepare_build_environment:
  extends: .docker
  stage: prepare
  script:
    - |
      docker build \
             --pull \
             --rm \
             --tag "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" \
             "./"
    - |
      docker run \
             --privileged \
             --rm \
             "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" \
             "/test/buildenv_check.sh"


# Testing
# ===========================================================================
.test:
  image: "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}"
  stage: test
  script: "test/${CI_JOB_NAME}.sh"

validate_chain:
  extends: .test

verify_update:
  extends: .test

start_update:
  extends: .test

system_update:
  extends: .test

boot_control:
  extends: .test

update_u-boot:
  extends: .test

updates.d/30_prepare_sfdisk:
  extends: .test

updates.d/40_update_rootfs:
  extends: .test

updates.d/90_update_boot_files:
  extends: .test

updates.d/95_update_boot_order:
  extends: .test

# Packaging
# ===========================================================================
.packaging:
  stage: package
  artifacts:
    paths:
      - "output/*/*/*.apk"
  script:
    - package_builder-alpine.sh

package_amd64:
  extends: .packaging
  image: "registry.gitlab.com/esbs/package_builder-alpine/amd64/package_builder-alpine:latest"

package_arm32v7:
  extends: .packaging
  image: "registry.gitlab.com/esbs/package_builder-alpine/arm32v7/package_builder-alpine:latest"

package_arm64v8:
  extends: .packaging
  image: "registry.gitlab.com/esbs/package_builder-alpine/arm64v8/package_builder-alpine:latest"

package_arm32v6:
  extends: .packaging
  image: "registry.gitlab.com/esbs/package_builder-alpine/arm32v6/package_builder-alpine:latest"

package_i386:
  extends: .packaging
  image: "registry.gitlab.com/esbs/package_builder-alpine/i386/package_builder-alpine:latest"


# Release branch
# ===========================================================================
create_release_branch:
  extends: .git
  stage: branch
  only:
    refs:
      - /^v\d+\.\d+$/
  except:
    refs:
      - branches # Workaround for gitlab-org/gitlab#27863
      - merge_requests
  script:
    - |
      CI_COMMIT_TAG_MESSAGE="$(git tag \
                                       --format="%(contents:subject)%0a%0a%(contents:body)" \
                                       --list ${CI_COMMIT_TAG:?})"
    - |
      if [ -z "${CI_COMMIT_TAG_MESSAGE:-}" ]; then
        CI_COMMIT_TAG_MESSAGE="$(printf "See tag '%s'\n\n(Auto-created release candidate)" "${CI_COMMIT_TAG:?}")"
      fi
    - git checkout "${CI_COMMIT_SHA}" -b "release/${CI_COMMIT_TAG:?}"
    - git tag --annotate --message="${CI_COMMIT_TAG_MESSAGE}" "${CI_COMMIT_TAG}.0-rc1"
    - git push --follow-tags origin "HEAD"


# Complete
# ===========================================================================
cleanup_docker_containers:
  extends: .docker
  stage: complete
  when: always
  script:
    - |
      if docker inspect --type image "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}" 1> "/dev/null"; then
        docker rmi "${CI_COMMIT_SHORT_SHA}:${CI_PIPELINE_ID}"
      fi
