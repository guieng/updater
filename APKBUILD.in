# Maintainer: Olliver Schinagl <oliver@schinagl.nl>
pkgname="esbs-updater"
pkgver="@PKGVER@"
pkgrel=0
pkgdesc="Update scripts for the Embedded Simple Board Support"
url="https://www.gitlab.com/esbs/updater"
arch="noarch"
license="AGPL-1.0-or-later"
depends="
	busybox
	e2fsprogs
	e2fsprogs-extra
	f2fs-tools
	mtd-utils-flash
	openssl
	sfdisk
	uboot-tools
"
_updatesd_sources="$(find "scripts/updates.d" -type f -executable -iname '[0-9][0-9]*.sh')"
source="
	scripts/boot_control.sh
	scripts/start_update.sh
	scripts/system_update.sh
	scripts/update_u-boot.sh
	scripts/validate_chain.sh
	scripts/verify_update.sh
	${_updatesd_sources}
"

check()
{
	for _script in "${source}"; do
		"${srcdir}/$(basename "${_script}")" -h 1> "/dev/null"
	done
}

package()
{
	install -D -m 755 -t "${pkgdir}/usr/bin/" \
	        "${srcdir}/validate_chain.sh" \
	        "${srcdir}/verify_update.sh"

	install -D -m 755 -t "${pkgdir}/usr/sbin" \
	        "${srcdir}/boot_control.sh" \
	        "${srcdir}/start_update.sh" \
	        "${srcdir}/system_update.sh" \
	        "${srcdir}/update_u-boot.sh"

	for _script in ${_updatesd_sources}; do
		if [ ! -x "${_script}" ]; then
			continue
		fi

		echo "Installing '${_script}' ..."
		install -D -m 755 \
		        -t "${pkgdir}/usr/libexec/system_update.d" \
		        "${_script}"
	done
}
