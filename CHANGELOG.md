# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


**NOTE:** DO NOT EDIT! This changelog is automatically generated. See the README.md file for more information.

## [v0.2.0] - 2020-04-17
### Fixes
- Negative offsets
- Post release fixes

### Added
- Support dual boot banks
- Trigger failure on worm
- Setup empty environments

## [v0.1.0] - 2020-02-18
### Added
- Validate chain of trust
- Initial update release
