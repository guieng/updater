# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright 2018 (C) Olliver Schinagl <oliver@schinagl.nl>

FROM registry.hub.docker.com/library/debian:testing-slim

LABEL Maintainer="Olliver Schinagl <oliver@schinagl.nl>"

# We want the latest stable version
# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
        e2fsprogs \
        easy-rsa \
        faketime \
        f2fs-tools \
        libubootenv-tool \
        shunit2 \
        squashfs-tools \
        u-boot-tools \
        util-linux \
        xxd \
        xz-utils \
    && \
    apt-get clean && \
    rm -rf \
        "/usr/share/doc" \
        "/usr/share/man" \
        "/var/lib/apt/lists/"

COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
