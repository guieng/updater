#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

set -eu

REQUIRED_COMMANDS="
	[
	command
	cp
	dd
	echo
	exit
	fallocate
	faketime
	fsck.ext4
	fsck.f2fs
	fw_printenv
	fw_setenv
	getopts
	head
	losetup
	mkenvimage
	mkfs.ext4
	mkfs.f2fs
	mksquashfs
	mkswap
	od
	openssl
	printf
	sed
	seq
	set
	sfdisk
	shift
	shuf
	shunit2
	stat
	tac
	tail
	test
	tr
	unlink
	unsquashfs
	xxd
	xz
"
REQUIRED_FILESYSTEMS="
	devpts
	devtmpfs
	ext4
	f2fs
	overlay
	proc
	squashfs
	sysfs
	tmpfs
"


usage()
{
	echo "Usage: ${0} [OPTIONS]"
	echo "A simple test script to verify the environment has all required tools."
	echo "    -h  Print usage"
}

check_loopdevice_support()
{
	printf "loop device support: "

	if ! PATH="${PATH}:/sbin:/usr/sbin:/usr/local/sbin" command losetup --version | \
	   grep -q "^losetup from util-linux"; then
		echo "error, full losetup from util-linux required"
		exit 1
	fi

	if ! PATH="${PATH}:/sbin:/usr/sbin:/usr/local/sbin" command losetup --find 1> "/dev/null"; then
	    echo "error, unable to get available loop device"
	    exit 1
	fi

	for _loopdevice in "/dev/loop"*; do
		if [ -b "${_loopdevice}" ]; then
			echo "ok"
			echo
			return
		fi
	done
	echo "error, no loopdevice"

	echo "Self-test failed, missing loop device support."
	exit 1
}

check_filesystem_support()
{
	if [ ! -f "/proc/filesystems" ]; then
		echo "Cannot read /proc/filesystems, cannot continue"
		exit 1
	fi

	for _fs in ${REQUIRED_FILESYSTEMS}; do
		if grep -q "${_fs}" "/proc/filesystems"; then
			echo "${_fs} support: ok"
		else
			_fs_test_result=1
			echo "${_fs} support: error"
		fi
	done
	echo

	if [ -n "${_fs_test_result:-}" ]; then
		echo "Self-test failed, missing filesystems."
		exit 1
	fi
}

check_easyrsa()
{
	printf "Easy-RSA: "

	if [ ! -x "/usr/share/easy-rsa/easyrsa" ] || \
	   ! "/usr/share/easy-rsa/easyrsa" "help" > "/dev/null"; then
		echo "not found"
		exit 1
	fi

	echo "ok"
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	echo "Passed tests:"
	# shellcheck disable=SC2059  # Interpret \n from variable
	printf "${_test_result_pass:-none\n}"
	echo
	echo "Failed tests:"
	# shellcheck disable=SC2059  # Interpret \n from variable
	printf "${_test_result_fail:-none\n}"
	echo

	if [ -n "${_test_result_fail:-}" ]; then
		echo "Self-test failed, missing dependencies."
		exit 1
	fi
}

main()
{
	while getopts ":h" options; do
		case "${options}" in
		h)
			usage
			exit 0
			;;
		:)
			echo "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			echo "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	check_requirements
	check_easyrsa
	check_filesystem_support
	check_loopdevice_support

	echo "All Ok"
}

main "${@}"

exit 0
