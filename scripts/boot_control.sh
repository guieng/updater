#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2020 Thibault Ferrante <thibault.ferrante@pm.me>

set -eu

COMPATIBLES_PATH="/sys/firmware/devicetree/base/compatible"
DEF_UPDATER_PATH="/usr/share/updater/"
DEF_UBOOT_PATH="/usr/share/u-boot/"
REQUIRED_COMMANDS="
	[
	command
	echo
	exit
	fw_printenv
	fw_setenv
	getopts
	mktemp
	printf
	return
	set
	tac
	test
	tr
	trap
"


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	>&2 echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} [OPTIONS]"
	echo
	echo "Get/Update boot information"
	echo "    -b  Get target bank"
	echo "    -d  Get target device"
	echo "    -h  Print this help text and exit"
	echo "    -p  Path to updater configuration files (default: '${DEF_UPDATER_PATH}') [UPDATER_PATH]"
	echo "    -s  Swap boot banks, boot from the alternative bank next reboot"
	echo "    -u  U-Boot board configuration files path (default: '${DEF_UBOOT_PATH}') [UBOOT_PATH]"
	echo "    -w  Get write count"
	echo "All options listed between [brackets] can also be passed in environment variables."
}

is_integer()
{
	__val="${1?Missing argument to function}"

	test "$((__val))" -eq "$((__val))" 2> "/dev/null"
}

cleanup()
{
	if [ -f "${script_file:-}" ]; then
		unlink "${script_file}"
	fi

	if [ -n "${unlocked_boot_device:-}" ]; then
		for _boot_device in ${unlocked_boot_device}; do
			lock_boot_device "${_boot_device}"
		done
	fi

	trap EXIT
}

init()
{
	trap cleanup EXIT

	script_file="$(mktemp -p "${TMPDIR:-/tmp}" "script_file.XXXXXX")"

	get_config "${uboot_path}" "board.config"
	get_config "${updater_path}" "boot.config"

	if ! get_write_count || [ "${write_count:=0}" -le 0 ]; then
		e_warn "Failed to get write counter, using defaults"
	fi

	if [ -z "${valid_configuration_file:-}" ]; then
		e_warn "Unable to determine valid environment"
	fi
}

get_config()
{
	_config_path=${1?Missing argument to function}
	_config_file_name=${2?Missing argument to function}

	if [ ! -f "${COMPATIBLES_PATH}" ]; then
		e_err "Unable to obtain 'compatible' node from the devicetree, is sysfs mounted at '/sys'?"
		exit 1
	fi

	for _compatible in $(tr '\0' '\n' < "${COMPATIBLES_PATH}" | tac); do
		if [ -n "${_compatible:-}" ] && \
		   [ -f "${_config_path}/${_compatible}/${_config_file_name}" ]; then
			# shellcheck source=/dev/null
			. "${_config_path}/${_compatible}/${_config_file_name}"
			_found_config="true"
		fi
	done

	if [ "${_found_config:-}" != "true" ]; then
		e_err "Unable to find a compatible '${_config_file_name}'."
		exit 1
	fi
}

get_write_count()
{
	for _config_env in ${UBOOT_ENV_CONFIG} ${UBOOT_ENV_COPY_CONFIG:-}; do
		if [ ! -f "${_config_env}" ] || \
		   [ ! -r "${_config_env}" ]; then
			e_warn "Unable to find '${_config_env}', skipping ..."
			continue
		fi

		if ! _write_count="$(fw_printenv --config "${_config_env}" --lock "/run/" --noheader "${ENVIRONMENT_WRITE_COUNT_NAME}")" || \
		   fw_printenv --config "${_config_env}" --lock "/run/" 2>&1 | \
		   grep -q "Bad CRC"; then
			# Even when the environment has a Bad CRC, fw_printenv exits with 0
			e_warn "Failure with boot environment '${_config_env}'"
			continue
		fi

		if ! is_integer "${_write_count:-false}" ; then
			_write_count=0
		fi

		if [ "${_write_count}" -gt "${write_count:-0}" ]; then
			write_count="${_write_count}"
			valid_configuration_file="${_config_env}"
		fi
	done

	if [ "${write_count:=0}" -le 0 ]; then
		return 1
	fi
}

get_env_var()
{
	_variable_name="${1?Missing argument to function}"
	_default_value="${2?Missing argument to function}"

	if [ -z "${valid_configuration_file:-}" ] || \
	   ! _var="$(fw_printenv --config "${valid_configuration_file:-}" \
                                 --lock "/run/" \
                                 --noheader \
                                 "${_variable_name}")" ;then
		_var="${_default_value}"
	fi

	printf "%s" "${_var}"
}

get_target_bank()
{
	if [ -z "${DEFAULT_BANK:-}" ] || \
	   [ -z "${ENVIRONMENT_BANK_ALT_VARIABLE_NAME:-}" ] || \
	   [ -z "${UBOOT_ENV_CONFIG:-}" ]; then
		e_err "Missing bank configuration, is 'boot.config' setup properly?"
		exit 1
	fi

	get_env_var "${ENVIRONMENT_BANK_ALT_VARIABLE_NAME}" "${DEFAULT_BANK}"
}

get_current_bank()
{
	if [ -z "${DEFAULT_BANK_ALT:-}" ] || \
	   [ -z "${ENVIRONMENT_BANK_VARIABLE_NAME:-}" ] || \
	   [ -z "${UBOOT_ENV_CONFIG:-}" ]; then
		e_err "Missing bank configuration, is 'boot.config' setup properly?"
		exit 1
	fi

	get_env_var "${ENVIRONMENT_BANK_VARIABLE_NAME}" "${DEFAULT_BANK_ALT}"
}

is_comment()
{
    test -z "${1%%#*}"
}

lock_boot_device()
{
	_target_device="${1?Missing argument to function}"

	if ! echo "1" > "/sys/block/$(basename "${_target_device}")/force_ro"; then
		e_warn "Failed to enable write protection on boot device '${_target_device}'"
		return
	fi
}

unlock_boot_device()
{
	_configuration_file="${1?Missing argument to function}"
	while read -r _path _; do
		if [ -z "${_path:-}" ] || is_comment "${_path}"; then
			continue
		fi

		if [ -L "${_path}" ]; then
			_path="$(readlink -f "${_path}")"
		fi

		if [ ! -b "${_path}" ]; then
			continue
		fi

		if ! echo "${_path}" | grep -E -q '^/dev/(block/)?mmcblk[[:digit:]]+boot[01]$'; then
			continue
		fi

		if ! echo "0" > "/sys/block/$(basename "${_path}")/force_ro"; then
			e_warn "Failed to disable write protection on boot device '${_path}'"
			continue
		fi

		unlocked_boot_device="${unlocked_boot_device:-} ${_path}"
	done < "${_configuration_file}"
}

update_single_env()
{
	_config_env="${1?Missing argument to function}"
	if [ ! -f "${_config_env}" ] || \
	   [ ! -r "${_config_env}" ]; then
		e_warn "Unable to find '${_config_env}', skipping ..."
		return 1
	fi

	for _ in $(seq 1 5); do
		unlock_boot_device "${_config_env}"
		if ! fw_setenv --config "${_config_env}" \
		               --lock "/run/" \
		               --script "${script_file}"; then
			e_warn "Failed to write environment into '${_config_env}', retrying ..."
			sleep 1
			continue
		fi

		while read -r _key _value; do
			if ! _verify="$(fw_printenv --config "${_config_env}" \
			                            --lock "/run" \
			                            --noheader \
			                            "${_key}")" || \
			   [ "${_verify:-}" != "${_value}" ]; then
				e_warn "Failed to verify environment value '${_key}': '${_verify}' != '${_value}', retrying ..."
				sleep 1
				continue
			fi
		done < "${script_file}"

		echo "Environment '${_config_env}' updated successfully"

		return 0
	done

	return 1
}

swap_banks()
{
	if [ -z "${ENVIRONMENT_BANK_VARIABLE_NAME:-}" ] || \
	   [ -z "${ENVIRONMENT_BANK_ALT_VARIABLE_NAME:-}" ] || \
	   [ -z "${UBOOT_ENV_CONFIG:-}" ]; then
		e_err "Missing bank configuration, is 'boot.config' setup properly?"
		exit 1
	fi

	_boot_env_updated=0
	_target_bank="$(get_target_bank)"
	_target_bank_alt="$(get_current_bank)"

	{
		printf "%s %s\n" "${ENVIRONMENT_BANK_VARIABLE_NAME}" "${_target_bank}"
		printf "%s %s\n" "${ENVIRONMENT_BANK_ALT_VARIABLE_NAME}" "${_target_bank_alt}"
		printf "%s %d\n" "${ENVIRONMENT_WRITE_COUNT_NAME}" "$((write_count + 1))"
	} > "${script_file}"

	for _env in ${UBOOT_ENV_CONFIG} ${UBOOT_ENV_COPY_CONFIG:-}; do
		if ! update_single_env "${_env}" ; then
			e_err "Unable to write on boot environment ${_env}, continuing any way"
		else
			_boot_env_updated="$((_boot_env_updated + 1))"
		fi
	done

	if [ "${_boot_env_updated}" -le 0 ]; then
		e_err "Unable to write on any boot environment"
		exit 1
	fi
}

get_target_device()
{
	if [ -z "${UPDATE_TARGET_DEVICE:-}" ]; then
		e_err "No UPDATE_TARGET_DEVICE available, is 'boot.config' setup properly?"
		exit 1
	fi

	printf "%s" "${UPDATE_TARGET_DEVICE}"
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err "Self-test failed, missing dependencies."
		echo "======================================="
		echo "Passed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_pass:-none\n}"
		echo "---------------------------------------"
		echo "Failed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_fail:-none\n}"
		echo "======================================="
		exit 1
	fi
}

main()
{
	while getopts ":bdhp:su:w" options; do
		case "${options}" in
		b)
			_action="get_target_bank"
			;;
		d)
			_action="get_target_device"
			;;
		h)
			usage
			exit 0
			;;
		p)
			updater_path="${OPTARG}"
			;;

		s)
			_action="swap_banks"
			;;
		u)
			uboot_path="${OPTARG}"
			;;
		w)
			_action="get_write_count"
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done

	uboot_path="${uboot_path:-${UBOOT_PATH:-${DEF_UBOOT_PATH}}}"
	updater_path="${updater_path:-${UPDATER_PATH:-${DEF_UPDATER_PATH}}}"

	check_requirements
	init

	case "${_action:-}" in
	get_target_device)
		get_target_device
		;;
	get_target_bank)
		get_target_bank
		;;
	get_write_count)
		printf "Write counter is currently at '%d'\n" "${write_count}"
		;;
	swap_banks)
		swap_banks
		;;
	*)
		e_err "Unknown option"
		usage
		exit 1
		;;
	esac
}

main "${@}"

exit 0
