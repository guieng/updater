#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

set -eu

SYSTEM_UPDATE_PATH="${SYSTEM_UPDATE_PATH:-/usr/libexec/system_update.d/}"
REQUIRED_COMMANDS="
	[
	break
	chroot
	command
	continue
	echo
	exit
	getopts
	grep
	mktemp
	mount
	mountpoint
	printf
	seq
	set
	shift
	test
	trap
	umount
	unlink
"


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} [OPTIONS] SWUPDATE_FILE"
	echo "Starts a firmware update by running scripts in a chroot from '${SYSTEM_UPDATE_PATH}'."
	echo "    -d Mandatory target storage device"
	echo "    -f Force failures where failures would be accepted (ex. WORM)"
	echo "    -h Print this help text and exit"
	echo
	echo "SWUPDATE_FILE, software update file to update with."
}

cleanup()
{
	for _ in $(seq 1 300); do
		if grep -q " $(readlink -f "${swupdate_file_mount:-}") " "/proc/mounts"; then
			umount "${swupdate_file_mount}"
		fi

		if [ -f "${swupdate_file_mount:-}" ]; then
			unlink "${swupdate_file_mount}"
		fi

		if mountpoint -q "${update_mount:-}/dev/pts"; then
			umount "${update_mount}/dev/pts"
		fi

		if mountpoint -q "${update_mount:-}/dev"; then
			umount "${update_mount}/dev"
		fi

		if mountpoint -q "${update_mount:-}/proc"; then
			umount "${update_mount}/proc"
		fi

		if mountpoint -q "${update_mount:-}/sys"; then
			umount "${update_mount}/sys"
		fi

		if mountpoint -q "${update_mount:-}/run"; then
			umount "${update_mount}/run"
		fi

		if mountpoint -q "${update_mount:-}/tmp"; then
			umount "${update_mount}/tmp"
		fi

		if ! mountpoint -q "${update_mount:-}/tmp"; then
			break
		fi

		e_warn "Failed to cleanup '${update_mount}', this can happen on slow media, retrying"
		sleep 1
	done

	if mountpoint -q "${update_mount:-}/tmp"; then
		e_warn "Unable to cleanup '${update_mount}', giving up."
	fi

	trap EXIT
}

init()
{
	trap cleanup EXIT

	echo "Initializing system software update using '${swupdate_file}'"

	_mount_attempts=0
	update_mount="${0}"
	while [ -n "${update_mount}" ] || [ "${_mount_attempts}" -lt 100 ]; do
		if mountpoint -q "${update_mount}"; then
			echo "Found our root directory at '${update_mount}'"
			break
		fi
		_mount_attempts="$((_mount_attempts + 1))"
		update_mount="${update_mount%/*}"
	done

	mount -t devtmpfs devfs "${update_mount}/dev"
	mount -t devpts devpts "${update_mount}/dev/pts"
	mount -t proc proc "${update_mount}/proc"
	mount -t sysfs sysfs "${update_mount}/sys"
	mount -t tmpfs tmpfs "${update_mount}/run"
	mount -t tmpfs tmpfs "${update_mount}/tmp"

	swupdate_file_mount="$(mktemp -p "${update_mount}/tmp" "swupdate_file_mount.XXXXXX")"
	mount -o bind "${swupdate_file}" "${swupdate_file_mount}"
}

run_update()
{
	echo "Updating system on device '${target_device}' bank '${target_bank}'"

	for _script in "${update_mount}/${SYSTEM_UPDATE_PATH}/"[0-9][0-9]_*".sh"; do
		if [ ! -x "${_script}" ]; then
			echo "Not an executable script '$(basename "${_script}")', skipping"
			continue
		fi
		_update_script="${_script#${update_mount}}"

		echo "Executing: '${_update_script}'"
		chroot "${update_mount}" \
			"${_update_script}" \
			-b "${target_bank}" \
			-d "${target_device}" \
			${force_failure:+-f} \
			"${swupdate_file_mount#${update_mount}}"
	done

	echo "System updated"
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err "Self-test failed, missing dependencies."
		echo "======================================="
		echo "Passed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_pass:-none\n}"
		echo "---------------------------------------"
		echo "Failed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_fail:-none\n}"
		echo "======================================="
		exit 1
	fi
}

main()
{
	while getopts ":d:fh" options; do
		case "${options}" in
		d)
			target_device="${OPTARG}"
			;;
		f)
			force_failure="true";
			;;
		h)
			usage
			exit 0
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	if [ "${#}" -lt 1 ]; then
		echo "Error: Missing parameter."
		usage
		exit 1
	fi

	if [ "${#}" -gt 1 ]; then
		echo "Error: Too many parameters."
		usage
		exit 1
	fi

	swupdate_file="${1:-}"

	check_requirements

	if [ ! -f "${swupdate_file}" ]; then
		e_err "Update '${swupdate_file}' is not a file"
		usage
		exit 1
	fi

	init

	if [ -z "${target_device:-}" ] && \
	   ! target_device="$(chroot "${update_mount}" "/usr/sbin/boot_control.sh" -d)"; then
		e_err "Unable to get default target storage device"
		exit 1
	fi

	if ! [ -b "${target_device:-}" ]; then
		e_err "Target storage device '${target_device:-}' is not a block device"
		usage
		exit 1
	fi

	if ! target_bank="$(chroot "${update_mount}" "/usr/sbin/boot_control.sh" -b)"; then
		e_err "Unable to get target bank"
		exit 1
	fi

	run_update
	cleanup
}

main "${@}"

exit 0
