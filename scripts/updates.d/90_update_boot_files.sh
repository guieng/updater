#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2017 Ultimaker B.V.
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2017 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>

set -eu

REQUIRED_COMMANDS="
	[
	break
	cmp
	cp
	command
	echo
	exit
	getopts
	mktemp
	mount
	mountpoint
	printf
	rmdir
	seq
	set
	shift
	sleep
	test
	umount
"


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} [OPTIONS]"
	echo "    -b Mandatory target bank"
	echo "    -d Mandatory target device"
	echo "    -f Force failures where failures would be accepted (ex. WORM)"
	echo "    -h Print this help text and exit"
	echo
	echo "Warning: This script is destructive and can destroy your data."
}

init()
{
	trap cleanup EXIT

	boot_partition_mountpoint="$(mktemp -d -p "${TMPDIR:-/tmp}" "boot_partition_mountpoint.XXXXXX")"
}

cleanup()
{
	if mountpoint -q "${boot_partition_mountpoint}"; then
	    umount "${boot_partition_mountpoint}"
	fi

	if [ -d "${boot_partition_mountpoint}" ]; then
	    rmdir "${boot_partition_mountpoint}"
	fi

	trap EXIT
}

update_kernel()
{
	_target_device="${1?Missing argument to function}"
	_rom_bank="boot_${2?Missing argument to function}"

	_boot_partition="$(sfdisk --dump --quiet "${_target_device}" | \
	                   sed -n 's|^[[:space:]]*\(/dev/[[:alnum:]]\+\)[[:space:]]*:.*name=[[:space:]]*\"\?'"${_rom_bank}"'\"\?.*$|\1|p')"

	if mountpoint -q "/boot"; then
		e_warn "A partition is mounted on '/boot', must assume kernel has been updated already"
		return
	fi

	mount "${_boot_partition}" "${boot_partition_mountpoint}"

	find "/boot/" | while read -r _boot_path; do
		_boot_path="${_boot_path#/boot/}"

		if [ -z "${_boot_path:-}" ] || \
		   [ "${_boot_path#lost+found}" != "${_boot_path}" ]; then
		    continue
		fi

		echo "Copying '${_boot_path}' to '${boot_partition_mountpoint}'"
		for _ in $(seq 1 10); do
			if [ -d "/boot/${_boot_path}" ]; then
				if mkdir -p "${boot_partition_mountpoint}/${_boot_path}"; then
					break
				fi
				e_warn "Failed to create directory, retrying ..."
				sleep 1
				continue
			fi

			if ! cp -a "/boot/${_boot_path}" "${boot_partition_mountpoint}/${_boot_path}"; then
				e_warn "Failed to copy file, retrying ..."
				continue
				sleep 1
			fi

			# Only compare files, as symlinks, sockets, pipes, etc can't be compared
			if [ -f "/boot/${_boot_path}" ] && \
			   [ ! -L "/boot/${_boot_path}" ] && \
			   cmp -s "/boot/${_boot_path}" "${boot_partition_mountpoint}/${_boot_path}"; then
				break
			elif [ -e "${boot_partition_mountpoint}/${_boot_path}" ] || \
			     [ -L "${boot_partition_mountpoint}/${_boot_path}" ]; then
				break
			fi

			e_warn "Failed to copy file, retrying ..."
			sleep 1
		done

		if [ -f "/boot/${_boot_path}" ] && [ ! -L "/boot/${_boot_path}" ] && \
		   cmp -s "/boot/${_boot_path}" "${boot_partition_mountpoint}/${_boot_path}"; then
			continue
		elif [ ! -e "${boot_partition_mountpoint}/${_boot_path}" ] && \
		     [ ! -L "${boot_partition_mountpoint}/${_boot_path}" ]; then
			e_err "Failed to properly copy file '${_boot_path}'"
			exit 1
		fi
	done

	find "${boot_partition_mountpoint}/" -depth | while read -r _boot_path; do
		if [ -e "/boot/${_boot_path#${boot_partition_mountpoint}}" ]; then
			continue
		fi

		if [ -d "${_boot_path}" ]; then
			if ! rmdir "${_boot_path}"; then
				e_warn "Unable to remove directory '${_boot_path}'"
			fi
			continue
		fi

		if ! rm -f "${_boot_path}"; then
			e_warn "Unable to remove '${_boot_path}'"
		fi
	done
}

update_uboot()
{
	if ! "update_u-boot.sh" ${force_failure:+-f}; then
		e_err "Failed to update U-Boot"
		exit 1
	fi
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err "Self-test failed, missing dependencies."
		echo "======================================="
		echo "Passed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_pass:-none\n}"
		echo "---------------------------------------"
		echo "Failed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_fail:-none\n}"
		echo "======================================="
		exit 1
	fi
}

main()
{
	while getopts ":b:d:fh" options; do
		case "${options}" in
		b)
			target_bank="${OPTARG}"
			;;
		d)
			target_device="${OPTARG}"
			;;
		f)
			force_failure="true";
			;;
		h)
			usage
			exit 0
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	if [ -z "${target_device:-}" ]; then
		e_err "Missing target storage device"
		usage
		exit 1
	fi

	if [ -z "${target_bank:-}" ] ; then
		e_err "Missing target bank"
		usage
		exit 1
	fi

	check_requirements
	init

	update_kernel "${target_device}" "${target_bank}"
	update_uboot
	cleanup
}

main "${@}"

exit 0
