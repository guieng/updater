#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2020 EVBox B.V.
# Copyright (C) 2020 Thibault Ferrante <thibault.ferrante@pm.me>

set -eu

REQUIRED_COMMANDS="
	[
	break
	cmp
	command
	echo
	exit
	getopts
	seq
	set
	shift
	test
"


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} [OPTIONS]"
	echo
	echo "Update the boot order following platform information"
	echo "    -h Print this help text and exit"
	echo
	echo "Warning: This script is destructive and can destroy your data."
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err "Self-test failed, missing dependencies."
		echo "======================================="
		echo "Passed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_pass:-none\n}"
		echo "---------------------------------------"
		echo "Failed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_fail:-none\n}"
		echo "======================================="
		exit 1
	fi
}

main()
{
	while getopts ":b:d:fh" options; do
		case "${options}" in
		b)
			echo "Boot bank is not relevant for '${0}', ignoring"
			;;
		d)
			echo "Target device is not relevant for '${0}', ignoring"
			;;
		f)
			echo "Failure flag not implemented for '${0}', ignoring"
			;;
		h)
			usage
			exit 0
			;;
		:)
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	check_requirements

	if ! "boot_control.sh" -s; then
		e_err "Unable to update the boot order"
		exit 1
	fi
}

main "${@}"

exit 0
