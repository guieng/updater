#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2020 Thibault Ferrante <thibault.ferrante@pm.me>

set -eu

METADATA_SIGNATURE_SIZE_BYTES=4
METADATA_SIGNATURE_SIZE_MAX=1024

METADATA_CHAIN_SIZE_BYTES=4
METADATA_CHAIN_SIZE_MAX="$((128 * 1024))"

REQUIRED_COMMANDS="
	[
	command
	echo
	exit
	getopts
	head
	mktemp
	od
	openssl
	printf
	set
	shift
	tail
	test
	tr
	trap
	unlink
"


e_err()
{
	>&2 echo "ERROR: ${*}"
}

e_warn()
{
	echo "WARN: ${*}"
}

usage()
{
	echo "Usage: ${0} -r ROOT_CA [-p PUBLIC_KEY] UPDATE_FILE"
	echo "Validates the chain of trust in the firmware update file."
	echo "    -h Print this help text and exit"
	echo "    -p Public key path for extraction"
	echo "    -r ROOT_CA certificate for verification"
	echo "    -v Print embedded certificates"
	echo
	echo "UPDATE_FILE, software update file to validate with."
}

cleanup()
{
	if [ -f "${certificate_chain:-}" ]; then
		unlink "${certificate_chain}"
	fi

	trap EXIT
}

init()
{
	trap cleanup EXIT

	if [ ! -f "${ROOT_CA:-}" ]; then
		e_err "Unable to read '${ROOT_CA:-ROOT_CA}' cannot continue"
		exit 1
	fi

	if [ ! -f "${update_file:-}" ]; then
		e_err "Unable to read '${update_file:-UPDATE_FILE}', cannot continue"
		exit 1
	fi

	certificate_chain="$(mktemp -p "${TMPDIR:-/tmp}" "certificate_chain.XXXXXX")"
}

extract_chain()
{
	signature_size="$((0x$(tail -c "${METADATA_SIGNATURE_SIZE_BYTES}" "${update_file}" | \
	                       od -An -t x4 | \
	                       tr -d " \t")))"
	if [ "${signature_size}" -gt "${METADATA_SIGNATURE_SIZE_MAX}" ] || \
	   [ "${signature_size}" -le 0 ]; then
		e_err "Unexpected signature size '${signature_size}'"
		exit 1
	fi
	_metadata_size="$((signature_size + METADATA_SIGNATURE_SIZE_BYTES))"

	_chain_size="$((0x$(head -c "-${_metadata_size}" "${update_file}" | \
	                   tail -c "${METADATA_CHAIN_SIZE_BYTES}" | \
	                   od -An -t x4 | \
	                   tr -d " \t")))"

	if [ "${_chain_size}" -gt "${METADATA_CHAIN_SIZE_MAX}" ] || \
	   [ "${_chain_size}" -le 0 ]; then
		e_err "Unexpected chain size '${_chain_size}'"
		exit 1
	fi

	_metadata_size="$((_metadata_size + METADATA_CHAIN_SIZE_BYTES))"
	_metadata_size="$((_metadata_size + _chain_size))"

	tail -c "${_metadata_size}" "${update_file}" | \
	head -c "${_chain_size}" > "${certificate_chain}"
}

validate_chain()
{
	if [ -n "${VERBOSE:-}" ]; then
		openssl crl2pkcs7 -nocrl -certfile "${certificate_chain}" | \
		openssl pkcs7 -print_certs -text -noout
	fi
	if ! openssl verify \
	                    -show_chain \
	                    -no-CApath \
	                    -CAfile "${ROOT_CA}" \
	                    -untrusted "${certificate_chain}" \
	                    "${certificate_chain}"; then
		e_err "Certificate chain not trusted"
		exit 1
	fi
}

extract_key()
{
	if ! openssl x509 -pubkey \
	                  -noout \
	                  -in "${certificate_chain}" \
	                  > "${SSL_PUBLIC_KEY}"; then
		e_err "Unable to extract the public key"
		exit 1
	fi
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err "Self-test failed, missing dependencies."
		echo "======================================="
		echo "Passed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_pass:-none\n}"
		echo "---------------------------------------"
		echo "Failed tests:"
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_fail:-none\n}"
		echo "======================================="
		exit 1
	fi
}

main()
{
	while getopts ":hp:r:v" _options; do
		case "${_options}" in
		h)
			usage
			exit 0
			;;
		p)
			SSL_PUBLIC_KEY="${OPTARG}"
			;;
		r)
			ROOT_CA="${OPTARG}"
			;;
		v)
			VERBOSE="TRUE"
			;;
		:)
			e_err "Option -${OPTARG} requires an argument"
			exit 1
			;;
		?)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	if [ "${#}" -lt 1 ]; then
		e_err "Missing argument"
		usage
		exit 1
	fi

	if [ "${#}" -gt 1 ]; then
		e_err "Too many arguments"
		usage
		exit 1
	fi

	update_file="${1}"

	check_requirements
	init
	extract_chain
	validate_chain
	if [ -n "${SSL_PUBLIC_KEY:-}" ]; then
		extract_key
	fi
	cleanup
}

main "${@}"

exit 0
