#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2018 EVBox B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2020 Thibault Ferrante <thibault.ferrante@pm.me>

set -eu

# Support any invocation of/with shunit2
if [ "${0}" = "${0%%shunit2}" ]; then
	"$(command -v shunit2)" "${0}"
	return "${?}"
fi
src_dir="${1%%${1##*/}}"
COMMAND_UNDER_TEST="${COMMAND_UNDER_TEST:-${src_dir}/../scripts/${1##*/}}"
shift

FIXTURES="${FIXTURES:-${src_dir}/fixtures/}"

# shellcheck source=test/common.inc.sh
. "${src_dir}/common.inc.sh"

set +eu


get_config()
{
	_compatibles="${1?Missing argument to function}"

	for _compatible in $(tr '\000' '\n' < "${_compatibles}" | tac); do
		if [ -n "${_compatible:-}" ] && \
		   [ -f "${FIXTURES}/updater/${_compatible}/boot.config" ]; then
			# shellcheck source=/dev/null
			. "${FIXTURES}/updater/${_compatible}/boot.config"
		fi
		if [ -n "${_compatible:-}" ] && \
		   [ -f "${FIXTURES}/u-boot/${_compatible}/board.config" ]; then
			# shellcheck source=/dev/null
			. "${FIXTURES}/u-boot/${_compatible}/board.config"
		fi
	done
}

cmp_boot_env()
{
	_mount_point="${1?}"
	_fw_config="${2?}"
	_main_bank_expected="${3?}"
	_alt_bank_expected="${4?}"
	_write_count_expected="${5?}"

	assertEquals "Main bank variable should target '${_main_bank_expected}'" \
	              "${_main_bank_expected}" \
	              "$(chroot "${_mount_point}" fw_printenv \
	                                                       --lock "/run" \
	                                                       --config "${_fw_config}" \
	                                                       --noheader \
	                                                       "${ENVIRONMENT_BANK_VARIABLE_NAME}")"

	assertEquals "Alternative bank variable should target '${_alt_bank_expected}'" \
	              "${_alt_bank_expected}" \
	              "$(chroot "${_mount_point}" fw_printenv \
	                                                       --lock "/run" \
	                                                       --config "${_fw_config}" \
	                                                       --noheader \
	                                                       "${ENVIRONMENT_BANK_ALT_VARIABLE_NAME}")"
	assertEquals "Write count should be correctly updated" \
	              "${_write_count_expected}" \
	              "$(chroot "${_mount_point}" fw_printenv \
	                                                       --lock "/run" \
	                                                       --config "${_fw_config}" \
	                                                       --noheader \
	                                                       "${ENVIRONMENT_WRITE_COUNT_NAME}")"
}

init_boot_env()
{
	echo "/boot/u-boot.env 0x0000 0x8000" > "${test_update_mountpoint}/${UBOOT_ENV_CONFIG}"
	mkdir -p "${test_update_mountpoint}/boot/"
	dd if="/dev/zero" of="${test_update_mountpoint}/boot/u-boot.env" bs=1024 count=32 > "/dev/null" 2>&1
	chroot "${test_update_mountpoint}" fw_setenv  --lock "/run" -c "${UBOOT_ENV_CONFIG}" env_version 0
}

set_boot_env()
{
	_mount_point="${1?}"
	_fw_config="${2?}"
	_main_bank_value="${3?}"
	_alt_bank_value="${4?}"
	_write_count="${5?}"
	chroot "${_mount_point}" fw_setenv \
	                               --lock "/run" \
	                               --config "${_fw_config}" \
	                               "${ENVIRONMENT_BANK_VARIABLE_NAME}" "${_main_bank_value}"

	chroot "${_mount_point}" fw_setenv \
	                               --lock "/run" \
	                               --config "${_fw_config}" \
	                               "${ENVIRONMENT_BANK_ALT_VARIABLE_NAME}" "${_alt_bank_value}"

	chroot "${_mount_point}" fw_setenv \
	                               --lock "/run" \
	                               --config "${_fw_config}" \
	                               "${ENVIRONMENT_WRITE_COUNT_NAME}" "${_write_count}"
}

oneTimeSetUp()
{
	echo "Setting up '${COMMAND_UNDER_TEST##*/}' test env"

	swupdate_src="$(mktemp -p "${SHUNIT_TMPDIR}" "swupdate_src.XXXXXX")"
	cp "${SOFTWARE_UPDATE_FILE}" "${swupdate_src}"

	test_squashfs_root_dir="$(mktemp -d -p "${SHUNIT_TMPDIR}" "test_squashfs_root_dir.XXXXXX")"
	squashfs_inject_path "${swupdate_src}" \
	                     "${FIXTURES}/updater/" \
	                     "/usr/share"
	squashfs_inject_path "${swupdate_src}" \
	                     "${FIXTURES}/u-boot/" \
	                     "/usr/share"
	squashfs_inject_path "${swupdate_src}" \
	                     "${COMMAND_UNDER_TEST}" \
	                     "/usr/sbin"
	COMMAND_UNDER_TEST="/usr/sbin/$(basename "${COMMAND_UNDER_TEST}")"

	echo
	echo "================================================================================"
}

oneTimeTearDown()
{
	echo "Tearing down '${COMMAND_UNDER_TEST##*/}' test env"

	if [ -d "${test_squashfs_root_dir:-}" ]; then
		rm -rf "${test_squashfs_root_dir?}"
	fi

	if [ -f "${swupdate_src}" ]; then
		unlink "${swupdate_src}"
	fi
}

setUp()
{
	echo

	unset ENVIRONMENT_BANK_ALT_VARIABLE_NAME
	unset ENVIRONMENT_BANK_VARIABLE_NAME
	unset ENVIRONMENT_WRITE_COUNT_NAME
	unset PLATFORM_TARGET_DEVICE
	unset UBOOT_ENV_CONFIG
	unset UBOOT_ENV_COPY_CONFIG

	test_swupdate_file="$(mktemp -p "${SHUNIT_TMPDIR}" "test_swupdate_file.XXXXXX")"
	cp "${swupdate_src}" "${test_swupdate_file}"


	test_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_target_device_img.XXXXXX")"
	create_dummy_storage_device "${test_target_device_img}"

	flock "${test_dummy_storage_device}" \
	      sfdisk --quiet "${test_dummy_storage_device}" < "${FIXTURES}/emmc_partition_table_template.sfdisk"

	# Do not store the update mount in the SHUNIT tmpdir, as unexpected failures
	# will trigger an rm -rf of the SHUNIT tmpdir, including the mounted /dev,
	# causing the host system to fail terribly. As neither tearDown nor
	# oneTimeTeardown is called; this will leave cruft in case of failures.
	# Until shunit2 implements a tearDown hook this cannot be avoided.
	test_update_mountpoint="$(mktemp -d -p "${TMPDIR:-/tmp}" "test_update_mountpoint.XXXXXX")"

	mount_chroot_test_env "${test_swupdate_file}" "${test_update_mountpoint}"
	install -D -d -m 655 \
	        "${test_update_mountpoint}/sys/firmware/devicetree/base"

	test_log_output="$(mktemp -p "${SHUNIT_TMPDIR}" "test_log_output.XXXXXX")"
}

tearDown()
{
	if [ -f "${test_log_output:-}" ]; then
		unlink "${test_log_output}"
	fi

	umount_chroot_test_env "${test_swupdate_file}" "${test_update_mountpoint}"

	destroy_dummy_storage_device "${test_target_device_img}"

	if [ -f "${test_swupdate_file:-}" ]; then
		unlink "${test_swupdate_file}"
	fi

	if mountpoint -q "${test_update_mountpoint:-}"; then
		umount "${test_update_mountpoint}"
	fi

	if [ -d "${test_update_mountpoint}" ]; then
		rmdir "${test_update_mountpoint}"
	fi

	echo "--------------------------------------------------------------------------------"
}

testGettingBootTargetDevice()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"\

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -d > "${test_log_output}"
	assertTrue "Target device should be found" "[ ${?} -eq 0 ]"

	assertEquals "Correct plaftorm should be used" \
	             "${UPDATE_TARGET_DEVICE}" \
	             "$(cat "${test_log_output}")"
}

testSwapTargetBankWithDualBoot()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed with default values" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "1"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the first flash" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "2"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the second flash" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "3"
}


testUpdateTargetBankWithFailingWrites()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "2"

	command_failure "${test_update_mountpoint}/usr/bin/fw_setenv"
	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertFalse "Update target bank should fail if fw_setenv fail" "[ ${?} -eq 0 ]"
}

testUpdateTargetBankWithTwoEnvironmentUpdated()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	mkdir -p "${test_update_mountpoint}/boot/"
	_i=1
	for _env in ${UBOOT_ENV_CONFIG} ${UBOOT_ENV_COPY_CONFIG:-}; do
		echo "/boot/u-boot.env${_i} 0x0000 0x8000" > "${test_update_mountpoint}/${_env}"
		dd if="/dev/zero" of="${test_update_mountpoint}/boot/u-boot.env${_i}" bs=1024 count=32 > "/dev/null" 2>&1
		chroot "${test_update_mountpoint}" fw_setenv --lock "/run" -c "${_env}" env_version 0
		_i="$((_i+1))"
	done

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed with default values" "[ ${?} -eq 0 ]"
	for _env in ${UBOOT_ENV_CONFIG} ${UBOOT_ENV_COPY_CONFIG:-}; do
		cmp_boot_env "${test_update_mountpoint}" "${_env}" "a" "b" "1"
	done

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the first flash" "[ ${?} -eq 0 ]"
	for _env in ${UBOOT_ENV_CONFIG} ${UBOOT_ENV_COPY_CONFIG:-}; do
		cmp_boot_env "${test_update_mountpoint}" "${_env}" "b" "a" "2"
	done

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the second flash" "[ ${?} -eq 0 ]"
	for _env in ${UBOOT_ENV_CONFIG} ${UBOOT_ENV_COPY_CONFIG:-}; do
		cmp_boot_env "${test_update_mountpoint}" "${_env}" "a" "b" "3"
	done
}

testUpdateTargetBankWithOneBrokenEnvironment()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env
	echo "/dev/null 0x0000 0x8000" > "${test_update_mountpoint}/${UBOOT_ENV_COPY_CONFIG}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed with default values" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "1"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the first flash" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "2"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed after the second flash" "[ ${?} -eq 0 ]"
	cmp_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "3"
}

testGettingTargetBankWithDualBoot()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	init_boot_env

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "Default bank device should be found without a boot env" "[ ${?} -eq 0 ]"
	assertEquals "Default bank should be used" \
	             "${DEFAULT_BANK}" \
	             "$(cat "${test_log_output}")"

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "1"
	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "Target bank should be found" "[ ${?} -eq 0 ]"
	assertEquals "Correct bank should be found" \
	             "b" \
	             "$(cat "${test_log_output}")"

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "b" "a" "2"
	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "Target bank should be found" "[ ${?} -eq 0 ]"
	assertEquals "Correct bank should be found" \
	             "a" \
	             "$(cat "${test_log_output}")"
}

testNoBootEnvSetupShouldUseDefault()
{
	printf "test,dual-boot-env\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "Default bank device should be found without a boot env" "[ ${?} -eq 0 ]"

	assertEquals "Default bank should be used" \
	             "${DEFAULT_BANK}" \
	             "$(cat "${test_log_output}")"
}

testNoCompatible()
{
	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -d > "${test_log_output}"
	assertFalse "Missing compatible should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b
	assertFalse "Missing compatible should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertFalse "Missing compatible should not be accepted." "[ ${?} -eq 0 ]"
}

testEmptyCompatible()
{
	printf "\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -d
	assertFalse "Empty compatible should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b
	assertFalse "Empty compatible should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertFalse "Empty compatible should not be accepted." "[ ${?} -eq 0 ]"
}

testInvalidCompatible()
{
	printf "test,no-valid-board\000" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -d
	assertFalse "Invalid compatible should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b
	assertFalse "Invalid compatible should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertFalse "Invalid compatible should not be accepted." "[ ${?} -eq 0 ]"
}

testUpdateTargetBankWithDifferentCounter()
{
	printf "test,dual-boot-env\0" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	get_config "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	mkdir -p "${test_update_mountpoint}/boot/"
	_i=1
	for _env in ${UBOOT_ENV_CONFIG} ${UBOOT_ENV_COPY_CONFIG:-}; do
		echo "/boot/u-boot.env${_i} 0x0000 0x8000" > "${test_update_mountpoint}/${_env}"
		dd \
		   bs=1024 \
		   count=32 \
		   if="/dev/zero" \
		   of="${test_update_mountpoint}/boot/u-boot.env${_i}" > "/dev/null" 2>&1
		chroot "${test_update_mountpoint}" fw_setenv --lock "/run" -c "${_env}" env_version 0
		_i="$((_i + 1))"
	done

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "1"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "2"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "The environment used should be the one with the highest count" "[ ${?} -eq 0 ]"

	assertEquals "Environment with the highest counter should be used" \
	             "a" \
	             "$(cat "${test_log_output}")"

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "2"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "1"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "The environment used should be the one with the highest count" "[ ${?} -eq 0 ]"

	assertEquals "Environment with the highest counter should be used" \
	             "b" \
	             "$(cat "${test_log_output}")"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -s
	assertTrue "Update target bank should succeed even when one environment is outdated" "[ ${?} -eq 0 ]"
	for _env in ${UBOOT_ENV_CONFIG} ${UBOOT_ENV_COPY_CONFIG:-}; do
		cmp_boot_env "${test_update_mountpoint}" "${_env}" "b" "a" "3"
	done

	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_CONFIG}" "a" "b" "2"
	set_boot_env "${test_update_mountpoint}" "${UBOOT_ENV_COPY_CONFIG}" "b" "a" "2"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -b > "${test_log_output}"
	assertTrue "The environment used should be the one the first with the highest count" "[ ${?} -eq 0 ]"

	assertEquals "The first environment with the highest counter should be used" \
	             "b" \
	             "$(cat "${test_log_output}")"
}
