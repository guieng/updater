#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>

set -eu

# Support any invocation of/with shunit2
if [ "${0}" = "${0%%shunit2}" ]; then
	"$(command -v shunit2)" "${0}"
	return "${?}"
fi
src_dir="${1%%${1##*/}}"
COMMAND_UNDER_TEST="${COMMAND_UNDER_TEST:-${src_dir}/../scripts/${1##*/}}"
shift

FIXTURES="${FIXTURES:-${src_dir}/fixtures/}"

# shellcheck source=test/common.inc.sh
. "${src_dir}/common.inc.sh"

set +eu


oneTimeSetUp()
{
	echo "Setting up '${COMMAND_UNDER_TEST##*/}' test env"

	swupdate_src="$(mktemp -p "${SHUNIT_TMPDIR}" "swupdate_src.XXXXXX")"
	cp "${SOFTWARE_UPDATE_FILE}" "${swupdate_src}"

	test_squashfs_root_dir="$(mktemp -d -p "${SHUNIT_TMPDIR}" "test_squashfs_root_dir.XXXXXX")"

	squashfs_inject_path "${swupdate_src}" \
	                     "${COMMAND_UNDER_TEST}" \
	                     "/usr/sbin/"
	COMMAND_UNDER_TEST="/usr/sbin/$(basename "${COMMAND_UNDER_TEST}")"

	echo
	echo "================================================================================"
}

oneTimeTearDown()
{
	echo "Tearing down '${COMMAND_UNDER_TEST##*/}' test env"

	if [ -d "${test_squashfs_root_dir:-}" ]; then
		rm -rf "${test_squashfs_root_dir?}"
	fi

	if [ -f "${swupdate_src}" ]; then
		unlink "${swupdate_src}"
	fi
}

setUp()
{
	echo

	test_swupdate_file="$(mktemp -p "${SHUNIT_TMPDIR}" "test_swupdate_file.XXXXXX")"
	cp "${swupdate_src}" "${test_swupdate_file}"

	test_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_device_img.XXXXXX")"
	create_dummy_storage_device "${test_target_device_img}"

	test_boot_control="$(mktemp -p "${SHUNIT_TMPDIR}" "test_boot_control.XXXXXX")"
	cp "${FIXTURES}/dummy_target_device.sh" "${test_boot_control}"

	test_update_mountpoint="$(mktemp -d -p "${SHUNIT_TMPDIR}" "test_update_mountpoint.XXXXXX")"
}

tearDown()
{
	destroy_dummy_storage_device "${test_target_device_img}"

	for _ in $(seq 1 300); do
		if [ -f "${test_swupdate_file:-}" ]; then
			unlink "${test_swupdate_file}"
		fi

		if mountpoint -q "${test_update_mountpoint:-}"; then
			umount "${test_update_mountpoint}"
		fi

		if [ -d "${test_update_mountpoint}" ]; then
			rmdir "${test_update_mountpoint}"
		fi

		if [ ! -d "${test_update_mountpoint}" ]; then
			break
		fi

		echo "Retrying cleanup of '${_update_mountpoint}', this can happen on slow media"
		sleep 1
	done

	if [ -d "${test_update_mountpoint}" ]; then
		echo "Giving up cleaning up '${_update_mountpoint}', manual cleanup may be needed."
	fi

	echo "--------------------------------------------------------------------------------"
}

testValidSWUpdateFile()
{
	squashfs_inject_path "${test_swupdate_file}" \
	                      "${FIXTURES}/scripts/00_update_success.sh" \
	                      "${SYSTEM_UPDATE_SCRIPT_DIR}/"
	squashfs_inject_path "${test_swupdate_file}" \
	                      "${FIXTURES}/scripts/10_update_success.sh" \
	                      "${SYSTEM_UPDATE_SCRIPT_DIR}/"

	sed "s'@TARGET@'${test_dummy_storage_device}'" "${FIXTURES}/dummy_target_device.sh" > "${test_boot_control}"
	chmod +x "${test_boot_control}"
	squashfs_inject_path "${test_swupdate_file}" \
	                     "${test_boot_control}" \
	                     "/usr/sbin/boot_control.sh"

	mount "${test_swupdate_file}" "${test_update_mountpoint}"
	assertTrue "Failed to mount update file" "[ ${?} -eq 0 ]"

	"${test_update_mountpoint}/${COMMAND_UNDER_TEST}" "${test_swupdate_file}"
	assertTrue "Correct update should be accepted." "[ ${?} -eq 0 ]"
}

testValidSWUpdateFileWithTargetOption()
{
	squashfs_inject_path "${test_swupdate_file}" \
	                      "${FIXTURES}/scripts/00_update_success.sh" \
	                      "${SYSTEM_UPDATE_SCRIPT_DIR}/"
	squashfs_inject_path "${test_swupdate_file}" \
	                      "${FIXTURES}/scripts/10_update_success.sh" \
	                      "${SYSTEM_UPDATE_SCRIPT_DIR}/"

	sed "s'@TARGET@'WRONG_DEFAULT'" "${FIXTURES}/dummy_target_device.sh" > "${test_boot_control}"
	chmod +x "${test_boot_control}"
	squashfs_inject_path "${test_swupdate_file}" \
	                     "${test_boot_control}" \
	                     "/usr/sbin/boot_control.sh"


	mount "${test_swupdate_file}" "${test_update_mountpoint}"
	assertTrue "Failed to mount update file" "[ ${?} -eq 0 ]"

	"${test_update_mountpoint}/${COMMAND_UNDER_TEST}" -d "${test_dummy_storage_device}" "${test_swupdate_file}"
	assertTrue "Correct update with target device as parameter should be accepted." "[ ${?} -eq 0 ]"
}

testValidSWUpdateFileWithoutPlatformInformation()
{
	squashfs_inject_path "${test_swupdate_file}" \
	                      "${FIXTURES}/scripts/00_update_success.sh" \
	                      "${SYSTEM_UPDATE_SCRIPT_DIR}/"
	squashfs_inject_path "${test_swupdate_file}" \
	                      "${FIXTURES}/scripts/10_update_success.sh" \
	                      "${SYSTEM_UPDATE_SCRIPT_DIR}/"

	command_failure "${test_boot_control}"
	chmod +x "${test_boot_control}"
	squashfs_inject_path "${test_swupdate_file}" \
	                     "${test_boot_control}" \
	                     "/usr/sbin/boot_control.sh"

	mount "${test_swupdate_file}" "${test_update_mountpoint}"
	assertTrue "Failed to mount update file" "[ ${?} -eq 0 ]"

	"${test_update_mountpoint}/${COMMAND_UNDER_TEST}" "${test_swupdate_file}"
	assertFalse "Failing to get compatible platform should fail the update." "[ ${?} -eq 0 ]"
}


testInsufficientParameters()
{
	mount "${test_swupdate_file}" "${test_update_mountpoint}"
	assertTrue "Failed to mount update file" "[ ${?} -eq 0 ]"

	"${test_update_mountpoint}/${COMMAND_UNDER_TEST}"
	assertFalse "Zero parameters is insufficient." "[ ${?} -eq 0 ]"

}

testExcessiveParameters()
{
	mount "${test_swupdate_file}" "${test_update_mountpoint}"
	assertTrue "Failed to mount update file" "[ ${?} -eq 0 ]"

	"${test_update_mountpoint}/${COMMAND_UNDER_TEST}" -d "${test_dummy_storage_device}" "${test_swupdate_file}" "dummy1"
	assertFalse "Excessive parameter should not be accepted." "[ ${?} -eq 0 ]"

	"${test_update_mountpoint}/${COMMAND_UNDER_TEST}" -d "${test_dummy_storage_device}" "${test_swupdate_file}" "dummy1" "dummy2"
	assertFalse "Excessive parameters should not be accepted." "[ ${?} -eq 0 ]"
}

testInvalidStorageDevice()
{
	mount "${test_swupdate_file}" "${test_update_mountpoint}"
	assertTrue "Failed to mount update file" "[ ${?} -eq 0 ]"

	sed "s'@TARGET@'/bin/true'" "${FIXTURES}/dummy_target_device.sh" > "${test_boot_control}"
	chmod +x "${test_boot_control}"
	squashfs_inject_path "${test_swupdate_file}" \
	                     "${test_boot_control}" \
	                     "/usr/sbin/boot_control.sh"

	"${test_update_mountpoint}/${COMMAND_UNDER_TEST}" "${test_swupdate_file}"
	assertFalse "Executable should not be accepted." "[ ${?} -eq 0 ]"

	sed "s'@TARGET@'/dev/console'" "${FIXTURES}/dummy_target_device.sh" > "${test_boot_control}"
	chmod +x "${test_boot_control}"
	squashfs_inject_path "${test_swupdate_file}" \
	                     "${test_boot_control}" \
	                     "/usr/sbin/boot_control.sh"

	"${test_update_mountpoint}/${COMMAND_UNDER_TEST}" "${test_swupdate_file}"
	assertFalse "Character device should not be accepted." "[ ${?} -eq 0 ]"

	sed "s'@TARGET@'/etc/issue'" "${FIXTURES}/dummy_target_device.sh" > "${test_boot_control}"
	chmod +x "${test_boot_control}"
	squashfs_inject_path "${test_swupdate_file}" \
	                     "${test_boot_control}" \
	                     "/usr/sbin/boot_control.sh"

	"${test_update_mountpoint}/${COMMAND_UNDER_TEST}" "${test_swupdate_file}"
	assertFalse "Text file should not be accepted." "[ ${?} -eq 0 ]"

	sed "s'@TARGET@'/tmp'" "${FIXTURES}/dummy_target_device.sh" > "${test_boot_control}"
	chmod +x "${test_boot_control}"
	squashfs_inject_path "${test_swupdate_file}" \
	                     "${test_boot_control}" \
	                     "/usr/sbin/boot_control.sh"

	"${test_update_mountpoint}/${COMMAND_UNDER_TEST}" "${test_swupdate_file}"
	assertFalse "Directory should not be accepted." "[ ${?} -eq 0 ]"
}

testFailedUpdateFirst()
{
	squashfs_inject_path "${test_swupdate_file}" \
	                      "${FIXTURES}/scripts/00_update_failed.sh" \
	                      "${SYSTEM_UPDATE_SCRIPT_DIR}/"
	squashfs_inject_path "${test_swupdate_file}" \
	                      "${FIXTURES}/scripts/10_update_success.sh" \
	                      "${SYSTEM_UPDATE_SCRIPT_DIR}/"

	mount "${test_swupdate_file}" "${test_update_mountpoint}"
	assertTrue "Failed to mount update file" "[ ${?} -eq 0 ]"

	"${test_update_mountpoint}/${COMMAND_UNDER_TEST}" -d "${test_dummy_storage_device}" "${test_swupdate_file}"
	assertFalse "Correct update should be accepted." "[ ${?} -eq 0 ]"
}

testFailedUpdateLast()
{
	squashfs_inject_path "${test_swupdate_file}" \
	                      "${FIXTURES}/scripts/00_update_success.sh" \
	                      "${SYSTEM_UPDATE_SCRIPT_DIR}/"
	squashfs_inject_path "${test_swupdate_file}" \
	                      "${FIXTURES}/scripts/10_update_failed.sh" \
	                      "${SYSTEM_UPDATE_SCRIPT_DIR}/"

	mount "${test_swupdate_file}" "${test_update_mountpoint}"
	assertTrue "Failed to mount update file" "[ ${?} -eq 0 ]"

	"${test_update_mountpoint}/${COMMAND_UNDER_TEST}" -d "${test_dummy_storage_device}" "${test_swupdate_file}"
	assertFalse "Correct update should be accepted." "[ ${?} -eq 0 ]"
}
