#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2018 EVBox B.V.
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
#
# Globally allow escape sequence interpretation due to \0 in devicetreenode
# shellcheck disable=SC2059

set -eu

# Support any invocation of/with shunit2
if [ "${0}" = "${0%%shunit2}" ]; then
	"$(command -v shunit2)" "${0}"
	return "${?}"
fi
src_dir="${1%%${1##*/}}"
COMMAND_UNDER_TEST="${COMMAND_UNDER_TEST:-${src_dir}/../scripts/${1##*/}}"
shift

FIXTURES="${FIXTURES:-${src_dir}/fixtures/}"

# shellcheck source=test/common.inc.sh
. "${src_dir}/common.inc.sh"

set +eu


get_config()
{
	_compatibles="${1?Missing argument to function}"

	for _compatible in $(echo "${_compatibles}" | tr '\000' '\n' | tac); do
		if [ -n "${_compatible:-}" ] && \
		   [ -f "${FIXTURES}/u-boot/${_compatible}/board.config" ]; then
			# shellcheck source=/dev/null
			. "${FIXTURES}/u-boot/${_compatible}/board.config"
		fi
	done
}

validate_boot_files()
{
	_input_file="${FIXTURES}/${1#/usr/share/}"
	_output_location="${2?Missing argument to function}"
	_byte_offset="${3:-0}"
	_dry_run="${4:+true}"

	_byte_size="$(stat -c "%s" "${_input_file}")"
	unset _test_boot_partition_mountpoint

	if [ -z "${_output_location%/boot/*}" ] && \
	   [ -n "${UBOOT_BOOT_DEVICE:-}" ]; then
		_boot_partition="${UBOOT_BOOT_DEVICE}"
		if [ "${_boot_partition%p?}" = "${_boot_partition}" ]; then
			_boot_bank="boot_a"
			_boot_partition="$(blkid | \
			                   sed -n 's|^[[:space:]]*\('"${test_dummy_storage_device}"'[[:alnum:]]\+\)[[:space:]]*:.*LABEL=[[:space:]]*\"\?'"${_boot_bank}"'\"\?.*$|\1|p')"
		fi
		_test_boot_partition_mountpoint="$(mktemp -d -p "${SHUNIT_TMPDIR}" "_test_boot_partition_mountpoint.XXXXXX")"
		mount "${test_update_mountpoint}/${_boot_partition}" "${_test_boot_partition_mountpoint}"
		_output_location="${_output_location#/boot/}"
	fi
	if [ "${_dry_run:-}" = "true" ]; then
		_output_location="/dev/zero"
	else
		_output_location="${_test_boot_partition_mountpoint:-${test_update_mountpoint}}/${_output_location}"
	fi

	if [ -b "${_output_location}" ] && [ "$((_byte_offset))" -lt 0 ]; then
		_byte_offset="$(($(blockdev --getsize64 "${_output_location}") - -_byte_offset))"
	fi

	if ! tail -c "+$((_byte_offset + 1))" "${_output_location}" | \
	   head -c "${_byte_size}" | \
	   cmp -s "${_input_file}"; then
		if [ "${_dry_run}" != "true" ]; then
			fail "File '${_input_file}' miss match"
		fi
	else
		if [ "${_dry_run}" = "true" ]; then
			fail "File '${_input_file}' matches"
		fi
	fi

	if mountpoint -q "${_test_boot_partition_mountpoint}"; then
		umount "${_test_boot_partition_mountpoint}"
	fi

	if [ -d "${_test_boot_partition_mountpoint}" ]; then
		rmdir "${_test_boot_partition_mountpoint}"
	fi
}

oneTimeSetUp()
{
	echo "Setting up '${COMMAND_UNDER_TEST##*/}' test env"

	swupdate_src="$(mktemp -p "${SHUNIT_TMPDIR}" "swupdate_src.XXXXXX")"
	cp "${SOFTWARE_UPDATE_FILE}" "${swupdate_src}"

	test_squashfs_root_dir="$(mktemp -d -p "${SHUNIT_TMPDIR}" "test_squashfs_root_dir.XXXXXX")"
	squashfs_inject_path "${swupdate_src}" \
	                     "${FIXTURES}/u-boot/" \
	                     "/usr/share"
	squashfs_inject_path "${swupdate_src}" \
	                     "${COMMAND_UNDER_TEST}" \
	                     "/usr/sbin"
	COMMAND_UNDER_TEST="/usr/sbin/$(basename "${COMMAND_UNDER_TEST}")"

	echo
	echo "================================================================================"
}

oneTimeTearDown()
{
	echo "Tearing down '${COMMAND_UNDER_TEST##*/}' test env"

	if [ -d "${test_squashfs_root_dir:-}" ]; then
		rm -rf "${test_squashfs_root_dir?}"
	fi

	if [ -f "${swupdate_src}" ]; then
		unlink "${swupdate_src}"
	fi
}

setUp()
{
	echo

	unset UBOOT_BOOT_DEVICE
	unset UBOOT_ENV_CONFIG
	unset UBOOT_SPL
	unset UBOOT_SPL_LOCATION
	unset UBOOT_SPL_OFFSET
	unset UBOOT_BIN
	unset UBOOT_BIN_LOCATION
	unset UBOOT_BIN_OFFSET
	unset UBOOT_ENV
	unset UBOOT_ENV_LOCATION
	unset UBOOT_ENV_OFFSET

	test_swupdate_file="$(mktemp -p "${SHUNIT_TMPDIR}" "test_swupdate_file.XXXXXX")"
	cp "${swupdate_src}" "${test_swupdate_file}"

	test_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_target_device_img.XXXXXX")"
	create_dummy_storage_device "${test_target_device_img}"

	flock "${test_dummy_storage_device}" \
	      sfdisk --quiet "${test_dummy_storage_device}" < "${FIXTURES}/emmc_partition_table_template.sfdisk"

	# Do not store the update mount in the SHUNIT tmpdir, as unexpected failures
	# will trigger an rm -rf of the SHUNIT tmpdir, including the mounted /dev,
	# causing the host system to fail terribly. As neither tearDown nor
	# oneTimeTeardown is called; this will leave cruft in case of failures.
	# Until shunit2 implements a tearDown hook this cannot be avoided.
	test_update_mountpoint="$(mktemp -d -p "${TMPDIR:-/tmp}" "test_update_mountpoint.XXXXXX")"

	mount_chroot_test_env "${test_swupdate_file}" "${test_update_mountpoint}"
	install -D -d -m 655 \
	        "${test_update_mountpoint}/sys/firmware/devicetree/base"
}

tearDown()
{
	umount_chroot_test_env "${test_swupdate_file}" "${test_update_mountpoint}"

	destroy_dummy_storage_device "${test_target_device_img}"

	if [ -f "${test_swupdate_file:-}" ]; then
		unlink "${test_swupdate_file}"
	fi

	if mountpoint -q "${test_update_mountpoint:-}"; then
		umount "${test_update_mountpoint}"
	fi

	if [ -d "${test_update_mountpoint}" ]; then
		rmdir "${test_update_mountpoint}"
	fi

	echo "--------------------------------------------------------------------------------"
}

testUpdateLime2BootFiles()
{
	_devicetreenode="olimex,a20-olinuxino-lime2-emmc\0allwinner,sun7i-a20\0"
	printf "${_devicetreenode}" > \
	       "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	get_config "${_devicetreenode}"

	ln -sf "${test_dummy_storage_device}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
	ln -sf "${test_dummy_storage_device}p${UBOOT_BOOT_DEVICE##*p}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
	ln -sf "${test_dummy_storage_device}p${UBOOT_ENV_LOCATION##*p}" \
	   "${test_update_mountpoint}/${UBOOT_ENV_LOCATION}"
	mkfs.ext4 -L "boot" "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"

	install -D -m 644 \
	        "${FIXTURES}/u-boot/olimex,a20-olinuxino-lime2-emmc/fw_env.config" \
	        "${test_update_mountpoint}/${UBOOT_ENV_CONFIG}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertTrue "Correct update boot files script should be accepted." "[ ${?} -eq 0 ]"

	validate_boot_files "${UBOOT_SPL}" \
	                    "${UBOOT_SPL_LOCATION}" \
	                    "${UBOOT_SPL_OFFSET}"

	assertEquals "Correct environment should have been created" \
	             "$(cat "${FIXTURES}/u-boot/olimex,a20-olinuxino-lime2-emmc/boardenv.txt")" \
	             "$(chroot "${test_update_mountpoint}" fw_printenv --lock "/run/")"

	unlink "${test_update_mountpoint}/${UBOOT_ENV_LOCATION}"
	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
}

testDryRunUpdateLime2BootFiles()
{
	_devicetreenode="olimex,a20-olinuxino-lime2-emmc\0allwinner,sun7i-a20\0"
	printf "${_devicetreenode}" > \
	       "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	get_config "${_devicetreenode}"

	ln -sf "${test_dummy_storage_device}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
	ln -sf "${test_dummy_storage_device}p${UBOOT_BOOT_DEVICE##*p}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
	ln -sf "${test_dummy_storage_device}p${UBOOT_ENV_LOCATION##*p}" \
	   "${test_update_mountpoint}/${UBOOT_ENV_LOCATION}"
	mkfs.ext4 -L "boot" "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"

	install -D -m 644 \
	        "${FIXTURES}/u-boot/olimex,a20-olinuxino-lime2-emmc/fw_env.config" \
	        "${test_update_mountpoint}/${UBOOT_ENV_CONFIG}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -y
	assertTrue "Correct update boot files script should be accepted." "[ ${?} -eq 0 ]"

	validate_boot_files "${UBOOT_SPL}" \
	                    "${UBOOT_SPL_LOCATION}" \
	                    "${UBOOT_SPL_OFFSET}" \
	                    "dry_run"

	assertFalse "Environment should not have been updated" \
	            "[ $(cat "${FIXTURES}/u-boot/olimex,a20-olinuxino-lime2-emmc/uEnv.txt") = \
	               $(chroot "${test_update_mountpoint}" fw_printenv --lock "/run/") ]"

	unlink "${test_update_mountpoint}/${UBOOT_ENV_LOCATION}"
	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
}

testUpdateBBBSBootFiles()
{
	_devicetreenode="ti,am335x-bone-black-spi\0ti,am335x-bone-black\0ti,am335x-bone\0ti,am33xx\0"
	printf "${_devicetreenode}" > \
	       "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	get_config "${_devicetreenode}"

	ln -sf "${test_dummy_storage_device}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
	ln -sf "${test_dummy_storage_device}p${UBOOT_BOOT_DEVICE##*p}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
	mkfs.ext4 -L "boot" "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"

	_test_storage_device="${test_dummy_storage_device}"

	_test_mtd_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_mtd_target_device_img.XXXXXX")"
	create_dummy_storage_device "${_test_mtd_target_device_img}" "${BOOT_MTD_FLASH_SIZE}"
	_test_mtd_target_device="${test_dummy_storage_device}"
	ln -sf "${_test_mtd_target_device}" \
	   "${test_update_mountpoint}/dev/mtd0"

	test_dummy_storage_device="${_test_storage_device}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertTrue "Correct update boot files script should be accepted." "[ ${?} -eq 0 ]"

	validate_boot_files "${UBOOT_SPL}" \
	                    "${UBOOT_SPL_LOCATION}" \
	                    "${UBOOT_SPL_OFFSET}"
	validate_boot_files "${UBOOT_BIN}" \
	                    "${UBOOT_BIN_LOCATION}" \
	                    "${UBOOT_BIN_OFFSET}"
	validate_boot_files "${UBOOT_ENV}" \
	                    "${UBOOT_ENV_LOCATION}" \
	                    "${UBOOT_ENV_OFFSET}"

	destroy_dummy_storage_device "${_test_mtd_target_device_img}"
	unlink "${test_update_mountpoint}/dev/mtd0"

	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
}

testUpdateBBBWBootFiles()
{
	_devicetreenode="ti,am335x-bone-black-wireless\0ti,am335x-bone-black\0ti,am335x-bone\0ti,am33xx\0"
	printf "${_devicetreenode}" > \
	       "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	get_config "${_devicetreenode}"

	ln -sf "${test_dummy_storage_device}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
	ln -sf "${test_dummy_storage_device}p${UBOOT_BOOT_DEVICE##*p}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
	mkfs.ext4 -L "boot" "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertTrue "Correct update boot files script should be accepted." "[ ${?} -eq 0 ]"

	validate_boot_files "${UBOOT_SPL}" \
	                    "${UBOOT_SPL_LOCATION}" \
	                    "${UBOOT_SPL_OFFSET}"
	validate_boot_files "${UBOOT_BIN}" \
	                    "${UBOOT_BIN_LOCATION}" \
	                    "${UBOOT_BIN_OFFSET}"
	validate_boot_files "${UBOOT_ENV}" \
	                    "${UBOOT_ENV_LOCATION}" \
	                    "${UBOOT_ENV_OFFSET}"

	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
}

testUpdateAlternativePath()
{
	_devicetreenode="ti,am335x-bone-black-wireless\0ti,am335x-bone-black\0ti,am335x-bone\0ti,am33xx\0"
	printf "${_devicetreenode}" > \
	       "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	get_config "${_devicetreenode}"

	ln -sf "${test_dummy_storage_device}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
	ln -sf "${test_dummy_storage_device}p${UBOOT_BOOT_DEVICE##*p}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
	mkfs.ext4 -L "boot" "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"

	mkdir -p "${test_update_mountpoint}/alternative_path/ti,am335x-bone-black-wireless/"
	mkdir -p "${test_update_mountpoint}/alternative_path/ti,am335x-bone-black/"
	mv "${test_update_mountpoint}/usr/share/u-boot/ti,am335x-bone-black-wireless/board.config" \
	   "${test_update_mountpoint}/alternative_path/ti,am335x-bone-black-wireless/"
	mv "${test_update_mountpoint}/usr/share/u-boot/ti,am335x-bone-black/board.config" \
	   "${test_update_mountpoint}/alternative_path/ti,am335x-bone-black/"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -u "/alternative_path"
	assertTrue "Correct update boot files script should be accepted." "[ ${?} -eq 0 ]"

	validate_boot_files "${UBOOT_SPL}" \
	                    "${UBOOT_SPL_LOCATION}" \
	                    "${UBOOT_SPL_OFFSET}"
	validate_boot_files "${UBOOT_BIN}" \
	                    "${UBOOT_BIN_LOCATION}" \
	                    "${UBOOT_BIN_OFFSET}"
	validate_boot_files "${UBOOT_ENV}" \
	                    "${UBOOT_ENV_LOCATION}" \
	                    "${UBOOT_ENV_OFFSET}"

	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
}

testSetupEmptyEnv()
{
	_devicetreenode="test,setup-empty-env\0"
	printf "${_devicetreenode}" > \
	        "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	get_config "${_devicetreenode}"

	install -D -m 755 \
	        "${FIXTURES}/u-boot/test,setup-empty-env/fw_env.config" \
		"${test_update_mountpoint}/${UBOOT_ENV_CONFIG}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -y
	assertTrue "Dry run of an empty environment should be accepted." "[ ${?} -eq 0 ]"
	assertEquals "Empty environment should not have been created." \
	             "" \
	             "$(chroot "${test_update_mountpoint}" fw_printenv --lock "/run/" | \
		        tail -n -1)"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertTrue "Setup of an empty environment should be accepted." "[ ${?} -eq 0 ]"
	assertEquals "Empty environment should have been created." \
	             "env_version=0" \
	             "$(chroot "${test_update_mountpoint}" fw_printenv --lock "/run/" | \
		        tail -n -1)"

	unlink "${test_update_mountpoint}/${UBOOT_ENV_LOCATION}"
	fallocate -l \
	          "$(tr -s ' \t' '\t' < "${FIXTURES}/u-boot/test,setup-empty-env/fw_env.config" | \
	             cut -f 3)" \
	          "${test_update_mountpoint}/${UBOOT_ENV_LOCATION}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertTrue "Setup of a corrupted environment should be accepted." "[ ${?} -eq 0 ]"
	assertEquals "Empty environment should have been created." \
	             "env_version=0" \
	             "$(chroot "${test_update_mountpoint}" fw_printenv --lock "/run/" | \
		        tail -n -1)"

	unlink "${test_update_mountpoint}/${UBOOT_ENV_LOCATION}"
	command_failure "${test_update_mountpoint}/usr/bin/mkenvimage"
	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertFalse "Failing mkenvimage should fail." "[ ${?} -eq 0 ]"

	command_placebo "${test_update_mountpoint}/usr/bin/mkenvimage"
	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertFalse "Unwritten environment should fail" "[ ${?} -eq 0 ]"
}

testUpdateNegativeOffsets()
{
	_devicetreenode="test,negative-offsets\0"
	printf "${_devicetreenode}" > \
	        "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	get_config "${_devicetreenode}"

	install -D -m 755 \
	        "${FIXTURES}/u-boot/test,negative-offsets/fw_env.config" \
		"${test_update_mountpoint}/${UBOOT_ENV_CONFIG}"

	ln -sf "${test_dummy_storage_device}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
	ln -sf "${test_dummy_storage_device}p${UBOOT_BOOT_DEVICE##*p}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertTrue "Correct update boot files with negative offsets should be accepted." "[ ${?} -eq 0 ]"

	validate_boot_files "${UBOOT_SPL}" \
	                    "${UBOOT_SPL_LOCATION}" \
	                    "${UBOOT_SPL_OFFSET}"

	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
}

testUpdateBootPartitions()
{
	_devicetreenode="test,boot-partitions\0"
	printf "${_devicetreenode}" > \
	        "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	get_config "${_devicetreenode}"

	install -D -d -m 755 \
	        "${test_update_mountpoint}/sys/block/$(basename "${UBOOT_SPL_LOCATION}")"
	ln -sf "${test_dummy_storage_device}" \
	   "${test_update_mountpoint}/${UBOOT_ENV_LOCATION}"
	ln -sf \
	   "${test_dummy_storage_device}p1" \
	   "${test_update_mountpoint}/${UBOOT_SPL_LOCATION?}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertTrue "Correct update boot files script should be accepted." "[ ${?} -eq 0 ]"

	validate_boot_files "${UBOOT_SPL}" \
	                    "${UBOOT_SPL_LOCATION}" \
	                    "${UBOOT_SPL_OFFSET}"
	validate_boot_files "${UBOOT_ENV}" \
	                    "${UBOOT_ENV_LOCATION}" \
	                    "${UBOOT_ENV_OFFSET}"

	unlink "${test_update_mountpoint}/${UBOOT_ENV_LOCATION}"
	unlink "${test_update_mountpoint}/${UBOOT_SPL_LOCATION}"
}

testUpdateBootFilesNamedPartitions()
{
	_devicetreenode="test,named-partitions\0"
	printf "${_devicetreenode}" > \
	        "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	_boot_bank="boot_a"
	_firmware_partition="$(sfdisk --dump --quiet "${test_dummy_storage_device}" | \
	                       sed -n 's|^[[:space:]]*\(/dev/[[:alnum:]]\+\)[[:space:]]*:.*name=[[:space:]]*\"\?'"${_boot_bank}"'\"\?.*$|\1|p')"
	_firmware_partition_num="${_firmware_partition##*p}"

	get_config "${_devicetreenode}"

	ln -sf "${test_dummy_storage_device}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE?}"
	ln -sf "${test_dummy_storage_device}p${_firmware_partition_num}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}p${_firmware_partition_num}"
	mkfs.ext4 -L "${_boot_bank}" \
	          "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}p${_firmware_partition_num}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertTrue "Correct update with named partitions should be accepted." "[ ${?} -eq 0 ]"

	validate_boot_files "${UBOOT_BIN}" \
	                    "${UBOOT_BIN_LOCATION}" \
	                    "${UBOOT_BIN_OFFSET}"
	validate_boot_files "${UBOOT_ENV}" \
	                    "${UBOOT_ENV_LOCATION}" \
	                    "${UBOOT_ENV_OFFSET}"

	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}p${_firmware_partition_num}"
	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
}

testDevicelessUpdateBootFiles()
{
	_devicetreenode="test,deviceless-boot\0"
	printf "${_devicetreenode}" > \
	        "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	get_config "${_devicetreenode}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertTrue "Correct update without device should be accepted." "[ ${?} -eq 0 ]"

	validate_boot_files "${UBOOT_BIN}" \
	                    "${UBOOT_BIN_LOCATION}" \
	                    "${UBOOT_BIN_OFFSET}"
	validate_boot_files "${UBOOT_ENV}" \
	                    "${UBOOT_ENV_LOCATION}" \
	                    "${UBOOT_ENV_OFFSET}"
}

testDryRunDevicelessUpdateBootFiles()
{
	_devicetreenode="test,deviceless-boot\0"
	printf "${_devicetreenode}" > \
	        "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	get_config "${_devicetreenode}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -y
	assertTrue "Correct update without device should be accepted." "[ ${?} -eq 0 ]"

	validate_boot_files "${UBOOT_BIN}" \
	                    "${UBOOT_BIN_LOCATION}" \
	                    "${UBOOT_BIN_OFFSET}" \
	                    "dry_run"
	validate_boot_files "${UBOOT_ENV}" \
	                    "${UBOOT_ENV_LOCATION}" \
	                    "${UBOOT_ENV_OFFSET}" \
	                    "dry_run"
}

testWriteFailureOnWORMLocations()
{
	_devicetreenode="test,write-fail\0"
	printf "${_devicetreenode}" > \
	        "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"
	_boot_bank="boot_a"
	_firmware_partition="$(sfdisk --dump --quiet "${test_dummy_storage_device}" | \
	                       sed -n 's|^[[:space:]]*\(/dev/[[:alnum:]]\+\)[[:space:]]*:.*name=[[:space:]]*\"\?'"${_boot_bank}"'\"\?.*$|\1|p')"
	_firmware_partition_num="${_firmware_partition##*p}"

	get_config "${_devicetreenode}"

	ln -sf "${test_dummy_storage_device}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE?}"
	ln -sf "${test_dummy_storage_device}p${_firmware_partition_num}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}p${_firmware_partition_num}"
	mkfs.ext4 -L "${_boot_bank}" \
	          "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}p${_firmware_partition_num}"

	command_failure "${test_update_mountpoint}/bin/dd"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertTrue "Write failures should not error on WORM locations." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" -f
	assertFalse "Write failures should error on WORM locations." "[ ${?} -eq 0 ]"

	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}p${_firmware_partition_num}"
	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
}

testNoCompatible()
{
	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertFalse "Missing compatible should not be accepted." "[ ${?} -eq 0 ]"
}

testEmptyCompatible()
{
	printf "\0" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertFalse "Empty compatible should not be accepted." "[ ${?} -eq 0 ]"
}

testInvalidCompatible()
{
	printf "test,no-valid-board\0" > "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertFalse "Invalid compatible should not be accepted." "[ ${?} -eq 0 ]"
}

testMissingBootFile()
{
	_devicetreenode="test,missing-file\0"
	printf "${_devicetreenode}" > \
	        "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertFalse "Missing files in config should not be accepted." "[ ${?} -eq 0 ]"
}

testDiskSizeFailure()
{
	_devicetreenode="olimex,a20-olinuxino-lime2-emmc\0allwinner,sun7i-a20\0"
	printf "${_devicetreenode}" > \
	        "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	command_failure "${test_update_mountpoint}/bin/stat"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertFalse "Missing files in config should not be accepted." "[ ${?} -eq 0 ]"
}

testCRCFailure()
{
	_devicetreenode="olimex,a20-olinuxino-lime2-emmc\0allwinner,sun7i-a20\0"
	printf "${_devicetreenode}" > \
	        "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	get_config "${_devicetreenode}"

	ln -sf "${test_dummy_storage_device}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
	ln -sf "${test_dummy_storage_device}p${UBOOT_BOOT_DEVICE##*p}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
	mkfs.ext4 -L "boot" "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"

	command_failure "${test_update_mountpoint}/usr/bin/md5sum"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertFalse "Failing CRC should not be accepted." "[ ${?} -eq 0 ]"

	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
}

testReadFailure()
{
	_devicetreenode="olimex,a20-olinuxino-lime2-emmc\0allwinner,sun7i-a20\0"
	printf "${_devicetreenode}" > \
	        "${test_update_mountpoint}/sys/firmware/devicetree/base/compatible"

	get_config "${_devicetreenode}"

	ln -sf "${test_dummy_storage_device}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
	ln -sf "${test_dummy_storage_device}p${UBOOT_BOOT_DEVICE##*p}" \
	   "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
	mkfs.ext4 -L "boot" "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"

	# The 'head' and 'tail' utilities are used to read the file from the target
	# medium. Neither of them erroring should produce proper results.
	command_failure "${test_update_mountpoint}/usr/bin/head"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertFalse "Failing head should not be accepted." "[ ${?} -eq 0 ]"

	if [ ! -x "${test_update_mountpoint}/bin/busybox" ]; then
		fail "Test environment has changed unexpectedly, missing busybox"
		exit 1
	fi
	ln -sf "/bin/busybox" \
	   "${test_update_mountpoint}/usr/bin/head"

	command_failure "${test_update_mountpoint}/usr/bin/tail"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertFalse "Failing tail should not be accepted." "[ ${?} -eq 0 ]"

	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE}"
	unlink "${test_update_mountpoint}/${UBOOT_BOOT_DEVICE%p?}"
}
