#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>

set -eu

# Support any invocation of/with shunit2
if [ "${0}" = "${0%%shunit2}" ]; then
	"$(command -v shunit2)" "${0}"
	return "${?}"
fi
src_dir="${1%%${1##*/}}"
COMMAND_UNDER_TEST="${COMMAND_UNDER_TEST:-${src_dir}/../../scripts/updates.d/${1##*/}}"
shift

FIXTURES="${FIXTURES:-${src_dir}/../fixtures/}"

# shellcheck source=test/common.inc.sh
. "${src_dir}/../common.inc.sh"

set +eu


partition_table_extract()
{
	_partition_table="${1?Missing argument to function}"

	# Produces a comma separated line containing only the device node, start
	# integer, size integer and name of a partition whilst ignoring any leading
	# zero's or empty partitions (size=0), e.g. "/dev/loop0p1,2048,16384,boot".
	echo "${_partition_table}" | while read -r line; do
		printf "%s" "${line}" | sed -n 's|^.*p.*\([[:digit:]]\+\)[[:space:]]:.*$|p\1,|p'
		printf "%s" "${line}" | sed -n 's|^.*start=[[:space:]]*\([1-9][[:digit:]]*\).*$|\1,|p'
		printf "%s" "${line}" | sed -n 's|^.*size=[[:space:]]*0*\([1-9][[:digit:]]*\).*$|\1,|p'
		printf "%s" "${line}" | sed -n 's|^.*name=[[:space:]]*\"\?\([[:alnum:]_]\+\)\"\?.*$|\1\n|p'
	done
}

validate_disk()
{
	_target_device="${1:?Missing argument to function}"
	_partition_table_disk_data="$(partition_table_extract "$(sfdisk --quiet --dump "${_target_device}" | sort -n)")"
	while IFS="," read -r _partition _ _ _name _; do
		if [ "${_name#boot*}" != "${_name}" ]; then
			_fsck_cmd="fsck.ext4 -f -n"
		elif [ "${_name#cache*}" != "${_name}" ] || \
		     [ "${_name#data*}" != "${_name}" ] || \
		     [ "${_name#log*}" != "${_name}" ] || \
		     [ "${_name#tmp*}" != "${_name}" ]; then
			_fsck_cmd="fsck.f2fs"
		else
			continue
		fi

		echo "Verifying partition '${_name}' on '${_target_device}${_partition}"
		if ! eval "${_fsck_cmd:?}" "${_target_device}${_partition}"; then
			fail "Partition contains errors"
		fi
	done <<-EOF
		${_partition_table_disk_data}
	EOF
}

validate_partition_table()
{
	_target_device="${1:?Missing argument to function}"
	_partition_file="${2:-${FIXTURES}/emmc_partition_table_template.sfdisk}"

	if ! sfdisk --list --verify "${_target_device}"; then
		fail "Partition tables have not been setup properly"
	fi

	_partition_table_disk_data="$(partition_table_extract "$(sfdisk --quiet --dump "${_target_device}" | sort -n)")"
	_partition_table_file_data="$(partition_table_extract "$(sort -n < "${_partition_file}")")"

	if [ "${_partition_table_disk_data}" != "${_partition_table_file_data}" ]; then
		fail "Partition table on '${_target_device}' does not match '${_partition_file}'"
	fi

	echo "Partition tables match"
}

add_partition_table_template()
{
	_update_mountpoint="${1?}"
	_test_storage_device="${2?}"
	_partition_table_template="${3:-${FIXTURES}/emmc_partition_table_template.sfdisk}"
	_test_base_target_device="$(basename "${_test_storage_device}")${4:-}"

	install -D -d -m 755 "${_update_mountpoint}/${SYSTEM_UPDATE_ETC_DIR}"
	sed 's|^part|'"${_test_storage_device}p"'|g' \
	    "${_partition_table_template}" > \
	    "${_update_mountpoint}/${SYSTEM_UPDATE_ETC_DIR}/${_test_base_target_device}.sfdisk"
	(
		cd "${_update_mountpoint}/${SYSTEM_UPDATE_ETC_DIR}"
		sha512sum "${_test_base_target_device}.sfdisk" > "${_test_base_target_device}.sfdisk.sha512"
	)
}

oneTimeSetUp()
{
	echo "Setting up '${COMMAND_UNDER_TEST##*/}' test env"

	swupdate_src="$(mktemp -p "${SHUNIT_TMPDIR}" "swupdate_src.XXXXXX")"
	cp "${SOFTWARE_UPDATE_FILE}" "${swupdate_src}"

	test_squashfs_root_dir="$(mktemp -d -p "${SHUNIT_TMPDIR}" "test_squashfs_root_dir.XXXXXX")"
	squashfs_inject_path "${swupdate_src}" \
	                     "${COMMAND_UNDER_TEST}" \
	                     "${SYSTEM_UPDATE_SCRIPT_DIR}"
	COMMAND_UNDER_TEST="${SYSTEM_UPDATE_SCRIPT_DIR}/$(basename "${COMMAND_UNDER_TEST}")"

	echo
	echo "================================================================================"
}

oneTimeTearDown()
{
	echo "Tearing down '${COMMAND_UNDER_TEST##*/}' test env"

	if [ -d "${test_squashfs_root_dir:-}" ]; then
		rm -rf "${test_squashfs_root_dir?}"
	fi

	if [ -f "${swupdate_src}" ]; then
		unlink "${swupdate_src}"
	fi
}

setUp()
{
	echo

	test_swupdate_file="$(mktemp -p "${SHUNIT_TMPDIR}" "test_swupdate_file.XXXXXX")"
	cp "${swupdate_src}" "${test_swupdate_file}"

	test_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_target_device_img.XXXXXX")"
	create_dummy_storage_device "${test_target_device_img}"

	# Do not store the update mount in the SHUNIT tmpdir, as unexpected failures
	# will trigger an rm -rf of the SHUNIT tmpdir, including the mounted /dev,
	# causing the host system to fail terribly. As neither tearDown nor
	# oneTimeTeardown is called; this will leave cruft in case of failures.
	# Until shunit2 implements a tearDown hook this cannot be avoided.
	test_update_mountpoint="$(mktemp -d -p "${TMPDIR:-/tmp}" "test_update_mountpoint.XXXXXX")"

	mount_chroot_test_env "${test_swupdate_file}" "${test_update_mountpoint}"
}

tearDown()
{
	umount_chroot_test_env "${test_swupdate_file}" "${test_update_mountpoint}"

	destroy_dummy_storage_device "${test_target_device_img}"

	if [ -f "${test_swupdate_file:-}" ]; then
		unlink "${test_swupdate_file}"
	fi

	if mountpoint -q "${test_update_mountpoint:-}"; then
		umount "${test_update_mountpoint}"
	fi

	if [ -d "${test_update_mountpoint}" ]; then
		rmdir "${test_update_mountpoint}"
	fi

	echo "--------------------------------------------------------------------------------"
}

testFormatEmptyDisk()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertTrue "Correct partitioning script should be accepted." "[ ${?} -eq 0 ]"
	validate_partition_table "${test_dummy_storage_device}"
	validate_disk "${test_dummy_storage_device}"
}

testFormatNormalAndHardwarePartitionsEmptyDisk()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}"

	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}" \
	                             "${FIXTURES}/emmc_gp0_partition_table_template.sfdisk" \
	                             "gp0"

	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}" \
	                             "${FIXTURES}/emmc_gp1_partition_table_template.sfdisk" \
	                             "gp1"

	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}" \
	                             "${FIXTURES}/emmc_boot1_partition_table_template.sfdisk" \
	                             "boot1"

	# It is not possible to create fake or dummy hardware partitions as used
	# per eMMC spec. So instead, symlink the loop device. Also it is a lot of
	# effort to also dynamically create these symlinks as the real script
	# creates the partitions which triggers the new nodes to appear, so lets
	# pre-create 3 symlinks. Note, to avoid having stale symlinks in /dev,
	# all links are put into /tmp instead. Note that due to the lack of a trap
	# hook in shunit2, crashing tests can cause cruft to remain (symlinks).
	_test_storage_device="${test_dummy_storage_device}"

	_test_gp0_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_gp0_target_device_img.XXXXXX")"
	create_dummy_storage_device "${_test_gp0_target_device_img}"
	_test_gp0_target_device="${test_dummy_storage_device}"
	ln -sf "${_test_gp0_target_device}" "${_test_storage_device}gp0"
	ln -sf "${_test_gp0_target_device}p1" "${_test_storage_device}gp0p1"
	ln -sf "${_test_gp0_target_device}p2" "${_test_storage_device}gp0p2"
	ln -sf "${_test_gp0_target_device}p3" "${_test_storage_device}gp0p3"

	_test_gp1_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_gp1_target_device_img.XXXXXX")"
	create_dummy_storage_device "${_test_gp1_target_device_img}"
	_test_gp1_target_device="${test_dummy_storage_device}"
	ln -sf "${_test_gp1_target_device}" "${_test_storage_device}gp1"
	ln -sf "${_test_gp1_target_device}p1" "${_test_storage_device}gp1p1"
	ln -sf "${_test_gp1_target_device}p2" "${_test_storage_device}gp1p2"
	ln -sf "${_test_gp1_target_device}p3" "${_test_storage_device}gp1p3"

	_test_boot1_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_boot1_target_device_img.XXXXXX")"
	create_dummy_storage_device "${_test_boot1_target_device_img}"
	_test_boot1_target_device="${test_dummy_storage_device}"
	ln -sf "${_test_boot1_target_device}" "${_test_storage_device}boot1"
	ln -sf "${_test_boot1_target_device}p1" "${_test_storage_device}boot1p1"
	ln -sf "${_test_boot1_target_device}p2" "${_test_storage_device}boot1p2"
	ln -sf "${_test_boot1_target_device}p3" "${_test_storage_device}boot1p3"

	test_dummy_storage_device="${_test_storage_device}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${_test_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertTrue "Correct partitioning script should be accepted." "[ ${?} -eq 0 ]"
	validate_partition_table "${_test_storage_device}"
	validate_disk "${_test_storage_device}"

	validate_partition_table "${_test_boot1_target_device}" \
	                         "${FIXTURES}/emmc_boot1_partition_table_template.sfdisk"
	validate_disk "${_test_boot1_target_device}"
	destroy_dummy_storage_device "${_test_boot1_target_device_img}"
	unlink "${_test_storage_device}boot1p3"
	unlink "${_test_storage_device}boot1p2"
	unlink "${_test_storage_device}boot1p1"
	unlink "${_test_storage_device}boot1"

	validate_partition_table "${_test_gp1_target_device}" \
	                         "${FIXTURES}/emmc_gp1_partition_table_template.sfdisk"
	validate_disk "${_test_gp1_target_device}"
	destroy_dummy_storage_device "${_test_gp1_target_device_img}"
	unlink "${_test_storage_device}gp1p3"
	unlink "${_test_storage_device}gp1p2"
	unlink "${_test_storage_device}gp1p1"
	unlink "${_test_storage_device}gp1"

	validate_partition_table "${_test_gp0_target_device}" \
	                         "${FIXTURES}/emmc_gp0_partition_table_template.sfdisk"
	validate_disk "${_test_gp0_target_device}"
	destroy_dummy_storage_device "${_test_gp0_target_device_img}"
	unlink "${_test_storage_device}gp0p3"
	unlink "${_test_storage_device}gp0p2"
	unlink "${_test_storage_device}gp0p1"
	unlink "${_test_storage_device}gp0"
}

testResizeDiskNotNeeded()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}"

	flock "${test_dummy_storage_device}" \
	      sfdisk --quiet "${test_dummy_storage_device}" < "${FIXTURES}/emmc_partition_table_template.sfdisk"
	_label_id="$(sfdisk --dump "${test_dummy_storage_device}" | \
	             sed -n 's|^label-id: \([[:alnum:]-]\+\)$|\1|p')"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertTrue "Correct partitioning script should be accepted." "[ ${?} -eq 0 ]"
	validate_partition_table "${test_dummy_storage_device}"
	assertEquals "Disk label-id's must be identical" "${_label_id}" \
	             "$(sfdisk --dump "${test_dummy_storage_device}" | \
	                sed -n 's|^label-id: \([[:alnum:]-]\+\)$|\1|p')"
}

testResizeDiskFormatNotNeeded()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}"

	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}" \
	                             "${FIXTURES}/emmc_gp0_partition_table_template.sfdisk" \
	                             "gp0"

	flock "${test_dummy_storage_device}" \
	      sfdisk --quiet "${test_dummy_storage_device}" < "${FIXTURES}/emmc_partition_table_template.sfdisk"
	_label_id="$(sfdisk --dump "${test_dummy_storage_device}" | \
	             sed -n 's|^label-id: \([[:alnum:]-]\+\)$|\1|p')"

	mkfs.ext4 -F -L "boot_a" -O ^metadata_csum "${test_dummy_storage_device}p1"
	_uuid_boot_a="$(blkid "${test_dummy_storage_device}p1" | \
	                sed -n 's|^.*\bUUID=\"\?\([[:alnum:]-]\+\)\"\?.*$|\1|p')"
	_mountpoint_boot_a="$(mktemp -d -p "${test_update_mountpoint}/tmp" "mountpoint_boot_a.XXXXXX")"
	mount "${test_dummy_storage_device}p1" "${_mountpoint_boot_a}"
	touch "${_mountpoint_boot_a}/testfile"
	tail --follow --pid="${$}" "${_mountpoint_boot_a}/testfile" &
	_pid_boot_a="${!}"

	mkfs.f2fs -f -l "data_a" "${test_dummy_storage_device}p3"
	_uuid_data_a="$(blkid "${test_dummy_storage_device}p3" | \
	                sed -n 's|^.*\bUUID=\"\?\([[:alnum:]-]\+\)\"\?.*$|\1|p')"
	_mountpoint_data_a="$(mktemp -d -p "${test_update_mountpoint}/tmp" "mountpoint_data_a.XXXXXX")"
	mount "${test_dummy_storage_device}p3" "${_mountpoint_data_a}"
	touch "${_mountpoint_data_a}/testfile"
	tail --follow --pid="${$}" "${_mountpoint_data_a}/testfile" &
	_pid_data_a="${!}"

	mkfs.ext4 -F -L "boot_b" -O ^metadata_csum "${test_dummy_storage_device}p4"
	_uuid_boot_b="$(blkid "${test_dummy_storage_device}p4" | \
	                sed -n 's|^.*\bUUID=\"\?\([[:alnum:]-]\+\)\"\?.*$|\1|p')"
	_mountpoint_boot_b="$(mktemp -d -p "${test_update_mountpoint}/tmp" "mountpoint_boot_b.XXXXXX")"
	mount "${test_dummy_storage_device}p4" "${_mountpoint_boot_b}"
	touch "${_mountpoint_boot_b}/testfile"
	tail --follow --pid="${$}" "${_mountpoint_boot_b}/testfile" &
	_pid_boot_b="${!}"

	mkfs.f2fs -f -l "data_b" "${test_dummy_storage_device}p6"
	_uuid_data_b="$(blkid "${test_dummy_storage_device}p6" | \
	                sed -n 's|^.*\bUUID=\"\?\([[:alnum:]-]\+\)\"\?.*$|\1|p')"
	_mountpoint_data_b="$(mktemp -d -p "${test_update_mountpoint}/tmp" "mountpoint_data_b.XXXXXX")"
	mount "${test_dummy_storage_device}p6" "${_mountpoint_data_b}"
	touch "${_mountpoint_data_b}/testfile"
	tail --follow --pid="${$}" "${_mountpoint_data_b}/testfile" &
	_pid_data_b="${!}"

	_test_storage_device="${test_dummy_storage_device}"

	_test_gp0_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_gp0_target_device_img.XXXXXX")"
	create_dummy_storage_device "${_test_gp0_target_device_img}"
	_test_gp0_target_device="${test_dummy_storage_device}"
	ln -sf "${_test_gp0_target_device}" "${_test_storage_device}gp0"
	ln -sf "${_test_gp0_target_device}p1" "${_test_storage_device}gp0p1"
	ln -sf "${_test_gp0_target_device}p2" "${_test_storage_device}gp0p2"

	flock "${_test_gp0_target_device}" \
	      sfdisk --quiet "${_test_gp0_target_device}" < "${FIXTURES}/emmc_gp0_partition_table_template.sfdisk"
	 _label_id_gp0="$(sfdisk --dump "${_test_gp0_target_device}" | \
	                  sed -n 's|^label-id: \([[:alnum:]-]\+\)$|\1|p')"

	mkfs.f2fs -f -l "tmp" "${_test_storage_device}gp0p1"
	_uuid_tmp="$(blkid "${_test_storage_device}gp0p1" | \
	             sed -n 's|^.*\bUUID=\"\?\([[:alnum:]-]\+\)\"\?.*$|\1|p')"
	_mountpoint_tmp="$(mktemp -d -p "${test_update_mountpoint}/tmp" "mountpoint_tmp.XXXXXX")"
	mount "${_test_storage_device}gp0p1" "${_mountpoint_tmp}"
	touch "${_mountpoint_tmp}/testfile"
	tail --follow --pid="${$}" "${_mountpoint_tmp}/testfile" &
	_pid_tmp="${!}"

	mkswap --label "swap" "${_test_storage_device}gp0p2"
	_uuid_swap="$(blkid "${_test_storage_device}gp0p2" | \
	              sed -n 's|^.*\bUUID=\"\?\([[:alnum:]-]\+\)\"\?.*$|\1|p')"

	test_dummy_storage_device="${_test_storage_device}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertTrue "Correct partitioning script should be accepted." "[ ${?} -eq 0 ]"

	assertEquals "Disk label-id should be the same" \
	             "${_label_id}" \
	             "$(sfdisk --dump "${test_dummy_storage_device}" | \
	                sed -n 's|^label-id: \([[:alnum:]-]\+\)$|\1|p')"
	assertEquals "UUID for 'boot_a' should be the same" \
	             "${_uuid_boot_a}" \
	             "$(blkid "${test_dummy_storage_device}p1" | \
	                sed -n 's|^.*\bUUID=\"\?\([[:alnum:]-]\+\)\"\?.*$|\1|p')"
	assertEquals "UUID for 'data_a' should be the same" \
	             "${_uuid_data_a}" \
	             "$(blkid "${test_dummy_storage_device}p3" | \
	                sed -n 's|^.*\bUUID=\"\?\([[:alnum:]-]\+\)\"\?.*$|\1|p')"
	assertEquals "UUID for 'boot_b' should be the same" \
	             "${_uuid_boot_b}" \
	             "$(blkid "${test_dummy_storage_device}p4" | \
	                sed -n 's|^.*\bUUID=\"\?\([[:alnum:]-]\+\)\"\?.*$|\1|p')"
	assertEquals "UUID for 'data_b' should be the same" \
	             "${_uuid_data_b}" \
	             "$(blkid "${test_dummy_storage_device}p6" | \
	                sed -n 's|^.*\bUUID=\"\?\([[:alnum:]-]\+\)\"\?.*$|\1|p')"

	assertEquals "Disk label-id for gp0 should be the same" \
	             "${_label_id_gp0}" \
	             "$(sfdisk --dump "${_test_storage_device}gp0" | \
	                sed -n 's|^label-id: \([[:alnum:]-]\+\)$|\1|p')"
	assertEquals "UUID for 'tmp' should be the same" \
	             "${_uuid_tmp}" \
	             "$(blkid "${_test_storage_device}gp0p1" | \
	                sed -n 's|^.*\bUUID=\"\?\([[:alnum:]-]\+\)\"\?.*$|\1|p')"
	assertEquals "UUID for 'swap' should be the same" \
	             "${_uuid_swap}" \
	             "$(blkid "${_test_storage_device}gp0p2" | \
	                sed -n 's|^.*\bUUID=\"\?\([[:alnum:]-]\+\)\"\?.*$|\1|p')"

	if kill -0 "${_pid_boot_a}"; then
		kill -9 "${_pid_boot_a}"
	fi
	if kill -0 "${_pid_data_a}"; then
		kill -9 "${_pid_data_a}"
	fi
	if kill -0 "${_pid_boot_b}"; then
		kill -9 "${_pid_boot_b}"
	fi
	if kill -0 "${_pid_data_b}"; then
		kill -9 "${_pid_data_b}"
	fi
	if kill -0 "${_pid_tmp}"; then
		kill -9 "${_pid_tmp}"
	fi
	sleep 1

	if mountpoint -q "${_mountpoint_boot_a}"; then
		umount "${_mountpoint_boot_a}"
	fi
	if [ -d "${_mountpoint_boot_a}" ]; then
		rmdir "${_mountpoint_boot_a}"
	fi

	if mountpoint -q "${_mountpoint_data_a}"; then
		umount "${_mountpoint_data_a}"
	fi
	if [ -d "${_mountpoint_data_a}" ]; then
		rmdir "${_mountpoint_data_a}"
	fi

	if mountpoint -q "${_mountpoint_boot_b}"; then
		umount "${_mountpoint_boot_b}"
	fi
	if [ -d "${_mountpoint_boot_b}" ]; then
		rmdir "${_mountpoint_boot_b}"
	fi

	if mountpoint -q "${_mountpoint_data_b}"; then
		umount "${_mountpoint_data_b}"
	fi
	if [ -d "${_mountpoint_data_b}" ]; then
		rmdir "${_mountpoint_data_b}"
	fi

	if mountpoint -q "${_mountpoint_tmp}"; then
		umount "${_mountpoint_tmp}"
	fi
	if [ -d "${_mountpoint_tmp}" ]; then
		rmdir "${_mountpoint_tmp}"
	fi

	validate_disk "${_test_storage_device}"
	validate_partition_table "${_test_storage_device}"

	validate_disk "${_test_gp0_target_device}"
	validate_partition_table "${_test_gp0_target_device}" \
	                         "${FIXTURES}/emmc_gp0_partition_table_template.sfdisk"

	destroy_dummy_storage_device "${_test_gp0_target_device_img}"
	unlink "${_test_storage_device}gp0p2"
	unlink "${_test_storage_device}gp0p1"
	unlink "${_test_storage_device}gp0"
}

testNosfdiskSupportedDevice()
{
	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertTrue "Update without sfdisk table should be accepted." "[ ${?} -eq 0 ]"
}

testResizeDisk()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}"

	flock "${test_dummy_storage_device}" \
	      sfdisk --quiet "${test_dummy_storage_device}" < "${FIXTURES}/testResizeDisk_emmc_partition_table_template.sfdisk"

	mkfs.ext4 -F -L "boot_a" -O ^metadata_csum "${test_dummy_storage_device}p1"

	mkfs.f2fs -f -l "data_a" "${test_dummy_storage_device}p3"

	mkfs.ext4 -F -L "boot_b" -O ^metadata_csum "${test_dummy_storage_device}p4"

	mkfs.f2fs -f -l "data_b" "${test_dummy_storage_device}p6"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertTrue "Correct partitioning script should be accepted." "[ ${?} -eq 0 ]"
	validate_partition_table "${test_dummy_storage_device}"
	validate_disk "${test_dummy_storage_device}"
}

testExistingSinglePartition()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}"

	flock "${test_dummy_storage_device}" \
	      sfdisk --quiet "${test_dummy_storage_device}" < "${FIXTURES}/testSinglePartition_partition_template.sfdisk"

	mkfs.ext4 -F -L "setup" "${test_dummy_storage_device}p1"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertTrue "Correct partitioning script should be accepted." "[ ${?} -eq 0 ]"
	validate_partition_table "${test_dummy_storage_device}"
	validate_disk "${test_dummy_storage_device}"
}

testFormatEmptyDiskAdditionalParameters()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")" \
	       "dummy1"
	assertFalse "Correct partitioning script with additional parameter should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")" \
	       "dummy1" "dummy2"
	assertFalse "Correct partitioning script with additional parameter should not be accepted." "[ ${?} -eq 0 ]"
}

testInsufficientParameters()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}"
	assertFalse "Zero parameters is insufficient." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Missing storage device option should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}"
	assertFalse "Missing update file option should not be accepted." "[ ${?} -eq 0 ]"
}

testInvalidStorageDevice()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "/bin/true" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Executable should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "/dev/console" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Character device should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "/etc/issue" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Text file should not be accepted." "[ ${?} -eq 0 ]"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "/tmp" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Directory should not be accepted." "[ ${?} -eq 0 ]"
}

testCorruptPartitionTable()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}"

	_corrupted_file="${test_update_mountpoint}/${SYSTEM_UPDATE_ETC_DIR}/$(basename "${_test_storage_device}").sfdisk"
	corrupt_file \
	             "${_corrupted_file}" \
	             "${_corrupted_file}" \
	             "$(random_int "$(($(stat -c "%s" "${_corrupted_file}") - 1))")" \
	             "random" \
	             1

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Corrupt partition table should not be accepted." "[ ${?} -eq 0 ]"
}

testCorruptPartitionTableSHA512()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}"

	_corrupted_file="${test_update_mountpoint}/${SYSTEM_UPDATE_ETC_DIR}/$(basename "${_test_storage_device}").sfdisk.sha512"
	corrupt_file \
	             "${_corrupted_file}" \
	             "${_corrupted_file}" \
	             "$(random_int "$(($(stat -c "%s" "${_corrupted_file}") - 1))")" \
	             "random" \
	             1

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Corrupt partition table should not be accepted." "[ ${?} -eq 0 ]"
}

testTooSmallBootPartition()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}" \
	                             "${FIXTURES}/testTooSmallBootPartition_emmc_partition_table_template.sfdisk"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Too small boot partition should not be accepted." "[ ${?} -eq 0 ]"
}

testTooSmallTrustStorePartition()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}" \
	                             "${FIXTURES}/testTooSmallTrustStorePartition_emmc_partition_table_template.sfdisk"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Too small trust store partition should not be accepted." "[ ${?} -eq 0 ]"
}

testTooSmallDataPartition()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}" \
	                             "${FIXTURES}/testTooSmallDataPartition_emmc_partition_table_template.sfdisk"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Too small data partition should not be accepted." "[ ${?} -eq 0 ]"
}

testTooSmallROMPartition()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}" \
	                             "${FIXTURES}/testTooSmallROMPartition_emmc_partition_table_template.sfdisk"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Too small ROM partition should not be accepted." "[ ${?} -eq 0 ]"
}

testOverlappingStartPartition()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}" \
	                             "${FIXTURES}/testOverlappingStartPartition_emmc_partition_table_template.sfdisk"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Overlapping start partition should not be accepted." "[ ${?} -eq 0 ]"
}

testOverlappingSizePartition()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}" \
	                             "${FIXTURES}/testOverlappingSizePartition_emmc_partition_table_template.sfdisk"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Overlapping size partition should not be accepted." "[ ${?} -eq 0 ]"
}

testOutOfBoundPartition()
{
	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}" \
	                             "${FIXTURES}/testOutOfBoundPartition_emmc_partition_table_template.sfdisk"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertFalse "Out of bound partition should not be accepted." "[ ${?} -eq 0 ]"
}

testCorruptedRepairableResizePartitions()
{
	# Corrupting the superblock is a tricky art. Overwriting all of it makes
	# it almost impossible to recover from. The values below introduce quite
	# a bit of havoc. Anything larger then this would require manual repair.
	# If the device where to be corrupted beyond this, very serious recovery
	# would be required.
	_ext4_superblock_offset=1024
	_ext4_superblock_size=48
	_f2fs_superblock_offset=0
	_f2fs_superblock_size=1024

	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}"

	flock "${test_dummy_storage_device}" \
	      sfdisk --quiet "${test_dummy_storage_device}" < "${FIXTURES}/testResizeDisk_emmc_partition_table_template.sfdisk"

	dd \
	   bs=1024 \
	   count=1024 \
	   if="/dev/urandom" \
	   of="${SHUNIT_TMPDIR}/random_test_data"

	_test_dummy_storage_device_mountpoint="$(mktemp -d -p "${SHUNIT_TMPDIR}" "test_dummy_storage_device.XXXXXX")"

	mkfs.ext4 -F -L "boot_a" -O ^metadata_csum "${test_dummy_storage_device}p1"
	mount "${test_dummy_storage_device}p1" "${_test_dummy_storage_device_mountpoint}"
	cp "${SHUNIT_TMPDIR}/random_test_data" "${_test_dummy_storage_device_mountpoint}/random_test_data"
	if ! cmp -l "${SHUNIT_TMPDIR}/random_test_data" "${_test_dummy_storage_device_mountpoint}/random_test_data"; then
		fail "Partition 'p1' random data does not match"
	fi
	umount "${test_dummy_storage_device}p1"
	dd \
	   bs=1 \
	   count="$(random_int_range 1 "${_ext4_superblock_size}")" \
	   if="/dev/zero" \
	   of="${test_dummy_storage_device}p1" \
	   seek="${_ext4_superblock_offset}"

	mkfs.f2fs -f -l "data_a" "${test_dummy_storage_device}p3"
	mount "${test_dummy_storage_device}p3" "${_test_dummy_storage_device_mountpoint}"
	cp "${SHUNIT_TMPDIR}/random_test_data" "${_test_dummy_storage_device_mountpoint}/random_test_data"
	umount "${test_dummy_storage_device}p3"
	dd \
	   bs=1 \
	   count="$(random_int_range 1 "${_f2fs_superblock_size}")" \
	   if="/dev/zero" \
	   of="${test_dummy_storage_device}p3" \
	   seek="${_f2fs_superblock_offset}"

	mkfs.ext4 -F -L "boot_b" -O ^metadata_csum "${test_dummy_storage_device}p4"
	mount "${test_dummy_storage_device}p4" "${_test_dummy_storage_device_mountpoint}"
	cp "${SHUNIT_TMPDIR}/random_test_data" "${_test_dummy_storage_device_mountpoint}/random_test_data"
	umount "${test_dummy_storage_device}p4"
	dd \
	   bs=1 \
	   count="$(random_int_range 1 "${_ext4_superblock_size}")" \
	   if="/dev/urandom" \
	   of="${test_dummy_storage_device}p4" \
	   seek="${_ext4_superblock_offset}"

	mkfs.f2fs -f -l "data_b" "${test_dummy_storage_device}p6"
	mount "${test_dummy_storage_device}p6" "${_test_dummy_storage_device_mountpoint}"
	cp "${SHUNIT_TMPDIR}/random_test_data" "${_test_dummy_storage_device_mountpoint}/random_test_data"
	umount "${test_dummy_storage_device}p6"
	dd \
	   bs=1 \
	   count="$(random_int_range 1 "${_f2fs_superblock_size}")" \
	   if="/dev/urandom" \
	   of="${test_dummy_storage_device}p6" \
	   seek="${_f2fs_superblock_offset}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertTrue "Correct partitioning script should be accepted." "[ ${?} -eq 0 ]"
	validate_partition_table "${test_dummy_storage_device}"
	validate_disk "${test_dummy_storage_device}"

	mount "${test_dummy_storage_device}p1" "${_test_dummy_storage_device_mountpoint}"
	if ! cmp -l "${SHUNIT_TMPDIR}/random_test_data" "${_test_dummy_storage_device_mountpoint}/random_test_data"; then
		fail "Partition 'p1' random data does not match"
	fi
	umount "${test_dummy_storage_device}p1"

	mount "${test_dummy_storage_device}p3" "${_test_dummy_storage_device_mountpoint}"
	if ! cmp -l "${SHUNIT_TMPDIR}/random_test_data" "${_test_dummy_storage_device_mountpoint}/random_test_data"; then
		fail "Partition 'p3' random data does not match"
	fi
	umount "${test_dummy_storage_device}p3"

	mount "${test_dummy_storage_device}p4" "${_test_dummy_storage_device_mountpoint}"
	if ! cmp -l "${SHUNIT_TMPDIR}/random_test_data" "${_test_dummy_storage_device_mountpoint}/random_test_data"; then
		fail "Partition 'p4' random data does not match"
	fi
	umount "${test_dummy_storage_device}p4"

	mount "${test_dummy_storage_device}p6" "${_test_dummy_storage_device_mountpoint}"
	if ! cmp -l "${SHUNIT_TMPDIR}/random_test_data" "${_test_dummy_storage_device_mountpoint}/random_test_data"; then
		fail "Partition 'p6' random data does not match"
	fi
	umount "${test_dummy_storage_device}p6"

	unlink "${SHUNIT_TMPDIR}/random_test_data"
	rmdir "${_test_dummy_storage_device_mountpoint}"
}

testFooBarResizePartitions()
{
	_ext4_superblock_offset=1024
	_ext4_superblock_size=1024
	_f2fs_superblock_offset=0
	_f2fs_superblock_size=3072

	add_partition_table_template \
	                             "${test_update_mountpoint}" \
	                             "${test_dummy_storage_device}"

	flock "${test_dummy_storage_device}" \
	      sfdisk --quiet "${test_dummy_storage_device}" < "${FIXTURES}/testResizeDisk_emmc_partition_table_template.sfdisk"

	mkfs.ext4 -F -L "boot_a" -O ^metadata_csum "${test_dummy_storage_device}p1"
	dd \
	   bs="${_ext4_superblock_size}" \
	   count=1 \
	   if="/dev/zero" \
	   of="${test_dummy_storage_device}p1" \
	   seek="${_ext4_superblock_offset}"

	mkfs.f2fs -f -l "data_a" "${test_dummy_storage_device}p3"
	dd \
	   bs="${_f2fs_superblock_size}" \
	   count=1 \
	   if="/dev/zero" \
	   of="${test_dummy_storage_device}p3" \
	   seek="${_f2fs_superblock_offset}"

	mkfs.ext4 -F -L "boot_b" -O ^metadata_csum "${test_dummy_storage_device}p4"
	dd \
	   bs="${_ext4_superblock_size}" \
	   if="/dev/urandom" \
	   of="${test_dummy_storage_device}p4" \
	   seek="${_ext4_superblock_offset}"

	mkfs.f2fs -f -l "data_b" "${test_dummy_storage_device}p6"
	dd \
	   bs="${_f2fs_superblock_size}" \
	   count=1 \
	   if="/dev/urandom" \
	   of="${test_dummy_storage_device}p6" \
	   seek="${_f2fs_superblock_offset}"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}" \
	       "/tmp/$(basename "${test_swupdate_file}")"
	assertTrue "Correct partitioning script should be accepted." "[ ${?} -eq 0 ]"
	validate_partition_table "${test_dummy_storage_device}"
	validate_disk "${test_dummy_storage_device}"
}
