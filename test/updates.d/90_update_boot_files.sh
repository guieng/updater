#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu

# Support any invocation of/with shunit2
if [ "${0}" = "${0%%shunit2}" ]; then
	"$(command -v shunit2)" "${0}"
	return "${?}"
fi
src_dir="${1%%${1##*/}}"
COMMAND_UNDER_TEST="${COMMAND_UNDER_TEST:-${src_dir}/../../scripts/updates.d/${1##*/}}"
shift

FIXTURES="${FIXTURES:-${src_dir}/../fixtures/}"

# shellcheck source=test/common.inc.sh
. "${src_dir}/../common.inc.sh"

set +eu


validate_boot_files()
{
	_boot_partition="${1?Missing argument to function}"

	_test_boot_partition_mountpoint="$(mktemp -d -p "${SHUNIT_TMPDIR}" "_test_boot_partition_mountpoint.XXXXXX")"
	mount "${_boot_partition}" "${_test_boot_partition_mountpoint}"

	if ! diff -q -r "${FIXTURES}/boot" "${_test_boot_partition_mountpoint}"; then
		fail "Boot partition and boot files miss-match"
	fi

	if mountpoint -q "${_test_boot_partition_mountpoint}"; then
		umount "${_test_boot_partition_mountpoint}"
	fi

	if [ -d "${_test_boot_partition_mountpoint}" ]; then
		rmdir "${_test_boot_partition_mountpoint}"
	fi
}

oneTimeSetUp()
{
	echo "Setting up '${COMMAND_UNDER_TEST##*/}' test env"

	swupdate_src="$(mktemp -p "${SHUNIT_TMPDIR}" "swupdate_src.XXXXXX")"
	cp "${SOFTWARE_UPDATE_FILE}" "${swupdate_src}"

	test_squashfs_root_dir="$(mktemp -d -p "${SHUNIT_TMPDIR}" "test_squashfs_root_dir.XXXXXX")"
	squashfs_inject_path "${swupdate_src}" \
	                     "${FIXTURES}/boot/" \
	                     "/boot"
	squashfs_inject_path "${swupdate_src}" \
	                     "${COMMAND_UNDER_TEST}" \
	                     "${SYSTEM_UPDATE_SCRIPT_DIR}"
	COMMAND_UNDER_TEST="${SYSTEM_UPDATE_SCRIPT_DIR}/$(basename "${COMMAND_UNDER_TEST}")"

	echo
	echo "================================================================================"
}

oneTimeTearDown()
{
	echo "Tearing down '${COMMAND_UNDER_TEST##*/}' test env"

	if [ -d "${test_squashfs_root_dir:-}" ]; then
		rm -rf "${test_squashfs_root_dir?}"
	fi

	if [ -f "${swupdate_src}" ]; then
		unlink "${swupdate_src}"
	fi
}

setUp()
{
	echo

	test_swupdate_file="$(mktemp -p "${SHUNIT_TMPDIR}" "test_swupdate_file.XXXXXX")"
	cp "${swupdate_src}" "${test_swupdate_file}"

	test_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_target_device_img.XXXXXX")"
	create_dummy_storage_device "${test_target_device_img}"

	flock "${test_dummy_storage_device}" \
	      sfdisk --quiet "${test_dummy_storage_device}" < "${FIXTURES}/emmc_partition_table_template.sfdisk"

	# Do not store the update mount in the SHUNIT tmpdir, as unexpected failures
	# will trigger an rm -rf of the SHUNIT tmpdir, including the mounted /dev,
	# causing the host system to fail terribly. As neither tearDown nor
	# oneTimeTeardown is called; this will leave cruft in case of failures.
	# Until shunit2 implements a tearDown hook this cannot be avoided.
	test_update_mountpoint="$(mktemp -d -p "${TMPDIR:-/tmp}" "test_update_mountpoint.XXXXXX")"

	mount_chroot_test_env "${test_swupdate_file}" "${test_update_mountpoint}"
	install -D -d -m 655 "${test_update_mountpoint}/sys/firmware/devicetree/base"
}

tearDown()
{
	umount_chroot_test_env "${test_swupdate_file}" "${test_update_mountpoint}"

	destroy_dummy_storage_device "${test_target_device_img}"

	if [ -f "${test_swupdate_file:-}" ]; then
		unlink "${test_swupdate_file}"
	fi

	if mountpoint -q "${test_update_mountpoint:-}"; then
		umount "${test_update_mountpoint}"
	fi

	if [ -d "${test_update_mountpoint}" ]; then
		rmdir "${test_update_mountpoint}"
	fi

	echo "--------------------------------------------------------------------------------"
}

testUpdateLime2BootFiles()
{
	install -D -m 755 \
	        "${FIXTURES}/scripts/00_update_success.sh" \
	        "${test_update_mountpoint}/usr/sbin/update_u-boot.sh"

	mkfs.ext4 -F -L "boot_a" -O ^metadata_csum "${test_dummy_storage_device}p1"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -b "a" \
	       -d "${test_dummy_storage_device}"
	assertTrue "Correct update boot files script should be accepted." "[ ${?} -eq 0 ]"

	validate_boot_files "${test_dummy_storage_device}p1"
}

testMissingTargetBank()
{
	install -D -m 755 \
	        "${FIXTURES}/scripts/00_update_success.sh" \
	        "${test_update_mountpoint}/usr/sbin/update_u-boot.sh"

	mkfs.ext4 -F -L "boot_a" -O ^metadata_csum "${test_dummy_storage_device}p1"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}"
	assertFalse "Missing target bank should fail" "[ ${?} -eq 0 ]"
}

testNoUpdateNeededPartition()
{
	install -D -m 755 \
	        "${FIXTURES}/scripts/00_update_success.sh" \
	        "${test_update_mountpoint}/usr/sbin/update_u-boot.sh"

	mkfs.ext4 -F -L "boot_a" -O ^metadata_csum "${test_dummy_storage_device}p1"

	_test_boot_partition_mountpoint="$(mktemp -d -p "${SHUNIT_TMPDIR}" "_test_boot_partition_mountpoint.XXXXXX")"
	mount "${_boot_partition}" "${_test_boot_partition_mountpoint}"

	if mountpoint -q "${_test_boot_partition_mountpoint}"; then
		cp -a -T "${FIXTURES}/boot" "${_test_boot_partition_mountpoint}"
		umount "${_test_boot_partition_mountpoint}"
	fi

	if [ -d "${_test_boot_partition_mountpoint}" ]; then
		rmdir "${_test_boot_partition_mountpoint}"
	fi

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -b "a" \
	       -d "${test_dummy_storage_device}"
	assertTrue "Update with files already in place should pass." "[ ${?} -eq 0 ]"

	validate_boot_files "${test_dummy_storage_device}p1"
}

testOutdatedFilesToBeUpdated()
{
	install -D -m 755 \
	        "${FIXTURES}/scripts/00_update_success.sh" \
	        "${test_update_mountpoint}/usr/sbin/update_u-boot.sh"

	mkfs.ext4 -F -L "boot_a" -O ^metadata_csum "${test_dummy_storage_device}p1"

	_test_boot_partition_mountpoint="$(mktemp -d -p "${SHUNIT_TMPDIR}" "_test_boot_partition_mountpoint.XXXXXX")"
	mount "${_boot_partition}" "${_test_boot_partition_mountpoint}"

	if mountpoint -q "${_test_boot_partition_mountpoint}"; then
		echo "Old kernel" > "${_test_boot_partition_mountpoint}/vmlinuz"
		echo "Random file" > "${_test_boot_partition_mountpoint}/random_file"
		mkdir -p "${_test_boot_partition_mountpoint}/empty_dir"
		echo "Old file" > "${_test_boot_partition_mountpoint}/old_file"

		umount "${_test_boot_partition_mountpoint}"
	fi

	if [ -d "${_test_boot_partition_mountpoint}" ]; then
		rmdir "${_test_boot_partition_mountpoint}"
	fi

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -b "a" \
	       -d "${test_dummy_storage_device}"
	assertTrue "Update with files already in place should be updated." "[ ${?} -eq 0 ]"

	validate_boot_files "${test_dummy_storage_device}p1"
}

testUBootUpdateFails()
{
	install -D -m 755 \
	        "${FIXTURES}/scripts/00_update_failed.sh" \
	        "${test_update_mountpoint}/usr/sbin/update_u-boot.sh"

	mkfs.ext4 -F -L "boot_a" -O ^metadata_csum "${test_dummy_storage_device}p1"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -b "a" \
	       -d "${test_dummy_storage_device}"
	assertFalse "Failing u-boot update should not be accepted." "[ ${?} -eq 0 ]"

	validate_boot_files "${test_dummy_storage_device}p1"
}

testUnformattedPartition()
{
	install -D -m 755 \
	        "${FIXTURES}/scripts/00_update_success.sh" \
	        "${test_update_mountpoint}/usr/sbin/update_u-boot.sh"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -b "a" \
	       -d "${test_dummy_storage_device}"
	assertFalse "Unformatted partition should not be able to copy." "[ ${?} -eq 0 ]"
}
