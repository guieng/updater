#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2018 Raymond Siudak <raysiudak@gmail.com>
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

set -eu

# Support any invocation of/with shunit2
if [ "${0}" = "${0%%shunit2}" ]; then
	"$(command -v shunit2)" "${0}"
	return "${?}"
fi
src_dir="${1%%${1##*/}}"
COMMAND_UNDER_TEST="${COMMAND_UNDER_TEST:-${src_dir}/../../scripts/updates.d/${1##*/}}"
shift

FIXTURES="${FIXTURES:-${src_dir}/../fixtures/}"

# shellcheck source=test/common.inc.sh
. "${src_dir}/../common.inc.sh"

set +eu


oneTimeSetUp()
{
	echo "Setting up '${COMMAND_UNDER_TEST##*/}' test env"

	swupdate_src="$(mktemp -p "${SHUNIT_TMPDIR}" "swupdate_src.XXXXXX")"
	cp "${SOFTWARE_UPDATE_FILE}" "${swupdate_src}"

	test_squashfs_root_dir="$(mktemp -d -p "${SHUNIT_TMPDIR}" "test_squashfs_root_dir.XXXXXX")"

	squashfs_inject_path "${swupdate_src}" \
	                     "${COMMAND_UNDER_TEST}" \
	                     "${SYSTEM_UPDATE_SCRIPT_DIR}"
	COMMAND_UNDER_TEST="${SYSTEM_UPDATE_SCRIPT_DIR}/$(basename "${COMMAND_UNDER_TEST}")"

	echo
	echo "================================================================================"
}

oneTimeTearDown()
{
	echo "Tearing down '${COMMAND_UNDER_TEST##*/}' test env"

	if [ -d "${test_squashfs_root_dir:-}" ]; then
		rm -rf "${test_squashfs_root_dir?}"
	fi

	if [ -f "${swupdate_src}" ]; then
		unlink "${swupdate_src}"
	fi
}

setUp()
{
	echo

	test_swupdate_file="$(mktemp -p "${SHUNIT_TMPDIR}" "test_swupdate_file.XXXXXX")"
	cp "${swupdate_src}" "${test_swupdate_file}"

	test_target_device_img="$(mktemp -p "${SHUNIT_TMPDIR}" "test_target_device_img.XXXXXX")"
	create_dummy_storage_device "${test_target_device_img}"

	flock "${test_dummy_storage_device}" \
	      sfdisk --quiet "${test_dummy_storage_device}" < "${FIXTURES}/emmc_partition_table_template.sfdisk"

	# Do not store the update mount in the SHUNIT tmpdir, as unexpected failures
	# will trigger an rm -rf of the SHUNIT tmpdir, including the mounted /dev,
	# causing the host system to fail terribly. As neither tearDown nor
	# oneTimeTeardown is called; this will leave cruft in case of failures.
	# Until shunit2 implements a tearDown hook this cannot be avoided.
	test_update_mountpoint="$(mktemp -d -p "${TMPDIR:-/tmp}" "test_update_mountpoint.XXXXXX")"

	mount_chroot_test_env "${test_swupdate_file}" "${test_update_mountpoint}"
	install -D -d -m 655 "${test_update_mountpoint}/sys/firmware/devicetree/base"
}

tearDown()
{
	umount_chroot_test_env "${test_swupdate_file}" "${test_update_mountpoint}"

	destroy_dummy_storage_device "${test_target_device_img}"

	if [ -f "${test_swupdate_file:-}" ]; then
		unlink "${test_swupdate_file}"
	fi

	if mountpoint -q "${test_update_mountpoint:-}"; then
		umount "${test_update_mountpoint}"
	fi

	if [ -d "${test_update_mountpoint}" ]; then
		rmdir "${test_update_mountpoint}"
	fi

	echo "--------------------------------------------------------------------------------"
}

testUpdateBootOrder()
{
	install -D -m 755 \
	        "${FIXTURES}/scripts/00_update_success.sh" \
	        "${test_update_mountpoint}/usr/sbin/boot_control.sh"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -b "a" \
	       -d "${test_dummy_storage_device}"
	assertTrue "Correct command line option should be accepted." "[ ${?} -eq 0 ]"
}

testMissingTargetBank()
{
	install -D -m 755 \
	        "${FIXTURES}/scripts/00_update_success.sh" \
	        "${test_update_mountpoint}/usr/sbin/boot_control.sh"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -d "${test_dummy_storage_device}"
	assertTrue "Missing target bank should not fail the update" "[ ${?} -eq 0 ]"
}

testMissingTargetDevice()
{
	install -D -m 755 \
	        "${FIXTURES}/scripts/00_update_success.sh" \
	        "${test_update_mountpoint}/usr/sbin/boot_control.sh"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -b "a"
	assertTrue "Missing target device should not fail the update" "[ ${?} -eq 0 ]"
}

testFailureUpdateBootOrder()
{
	install -D -m 755 \
	        "${FIXTURES}/scripts/00_update_failed.sh" \
	        "${test_update_mountpoint}/usr/sbin/boot_control.sh"

	chroot "${test_update_mountpoint}" "${COMMAND_UNDER_TEST}" \
	       -b "a" \
	       -d "${test_dummy_storage_device}"
	assertFalse "Failure to update boot order should fail the update" "[ ${?} -eq 0 ]"
}
