#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2020 Thibault Ferrante <thibault.ferrante@pm.me>

set -eu

# Support any invocation of/with shunit2
if [ "${0}" = "${0%%shunit2}" ]; then
	"$(command -v shunit2)" "${0}"
	return "${?}"
fi
src_dir="${1%%${1##*/}}"
COMMAND_UNDER_TEST="${COMMAND_UNDER_TEST:-${src_dir}/../scripts/${1##*/}}"
shift

FIXTURES="${FIXTURES:-${src_dir}/fixtures/}"

# shellcheck source=test/common.inc.sh
. "${src_dir}/common.inc.sh"

set +eu


prepare_swu()
{
	_swu_src="${1?}"
	_private_key_signing="${2?}"
	_certificate_chain="${3?}"

	append_payload_marker "${_swu_src}"
	append_metadata "${_swu_src}" "${_certificate_chain}"
	sign_files "${_private_key_signing}" "${_swu_src}"
	append_metadata "${_swu_src}" "${_swu_src}.sig"
}

create_firmware_certificate()
{
	_pki_dir="${1?}"
	_certificate="${2?}"
	_test_certificate="${3?}"
	_test_private_key="${4:-}"
	_test_public_key="${5:-}"
	_test_public_key_der="${6:-}"

	(
		cd "${_pki_dir}"
		EASYRSA_REQ_CN="ESBS Updater C.I. testing software update signing certificate" \
			./easyrsa --batch gen-req "${_certificate}" nopass
		./easyrsa --batch sign-req code-signing "${_certificate}"
		./easyrsa "show-cert" "${_certificate}"
	)

	if [ ! -f "${_pki_dir}/pki/issued/${_certificate}.crt" ]; then
		fail "Failed to create test certificate"
	fi
	cp "${_pki_dir}/pki/issued/${_certificate}.crt" "${_test_certificate}"

	if [ ! -f "${_pki_dir}/pki/private/${_certificate}.key" ]; then
		fail "Failed to create private test key"
	fi

	if [ -n "${_test_private_key:-}" ]; then
		cp "${_pki_dir}/pki/private/${_certificate}.key" "${_test_private_key}"
	fi


	if [ -z "${_test_public_key:-}" ]; then
		return
	fi

	openssl x509 \
	        -in "${_test_certificate}" \
	        -out "${_test_public_key}" \
	        -noout \
	        -pubkey

	if [ ! -f "${_test_public_key}" ]; then
		fail "Failed to create public test key"
	fi
	if [ -n "${_test_public_key_der:-}" ]; then
		openssl rsa -pubin -in "${_test_public_key}" -outform der -out "${_test_public_key_der}"
	fi
}

sign_sub_ca()
{
	_pki_root_dir="${1?}"
	_pki_swu_dir="${2?}"

	(
		cd "${_pki_root_dir}"
		./easyrsa --batch import-req "${_pki_swu_dir}/pki/reqs/ca.req" "swu_ca"
		./easyrsa --batch sign-req ca "swu_ca"
	)
	cat "${_pki_root_dir}/pki/issued/swu_ca.crt" \
	    "${_pki_root_dir}/pki/ca.crt" > "${_pki_swu_dir}/pki/ca.crt"
	(
		cd "${_pki_swu_dir}"
		./easyrsa show-ca
		./easyrsa gen-crl
	)
}

create_sub_ca()
{
	_pki_dir="${1?}"
	_cn_suffix="${2?}"

	rm -rf "${_pki_dir}/pki"
	(
		cd "${_pki_dir}"
		./easyrsa init-pki
		EASYRSA_REQ_CN="ESBS Updater C.I. ${_cn_suffix}" ./easyrsa --batch build-ca nopass subca
	)
}

create_root_ca()
{
	_pki_dir="${1?}"
	rm -rf "${_pki_dir}/pki"

	(
		cd "${_pki_dir}"
		./easyrsa init-pki
		EASYRSA_REQ_CN="ESBS Updater C.I. testing root CA" ./easyrsa --batch build-ca nopass
		./easyrsa gen-crl
		./easyrsa show-ca
	)
}

oneTimeSetUp()
{
	echo "Setting up '${COMMAND_UNDER_TEST##*/}' test env"

	swupdate_src="$(mktemp -p "${SHUNIT_TMPDIR}" "swupdate_src.XXXXXX")"
	cp "${SOFTWARE_UPDATE_FILE}" "${swupdate_src}"

	echo "Generating PKI"
	cp -a "${FIXTURES}/PKI" "${SHUNIT_TMPDIR}"

	echo "Generating Root_CA"
	cp -a "${SHUNIT_TMPDIR}/PKI/CA" "${SHUNIT_TMPDIR}/PKI/root_CA"
	create_root_ca "${SHUNIT_TMPDIR}/PKI/root_CA"

	echo "Generating L1_CA"
	cp -a "${SHUNIT_TMPDIR}/PKI/CA" "${SHUNIT_TMPDIR}/PKI/L1_CA"
	create_sub_ca "${SHUNIT_TMPDIR}/PKI/L1_CA" "L1 certificate"
	sign_sub_ca "${SHUNIT_TMPDIR}/PKI/root_CA" "${SHUNIT_TMPDIR}/PKI/L1_CA"

	echo "Generating L2_CA"
	cp -a "${SHUNIT_TMPDIR}/PKI/CA" "${SHUNIT_TMPDIR}/PKI/L2_CA"
	create_sub_ca "${SHUNIT_TMPDIR}/PKI/L2_CA" "L2 certificate"
	sign_sub_ca "${SHUNIT_TMPDIR}/PKI/L1_CA" "${SHUNIT_TMPDIR}/PKI/L2_CA"

	echo "Generating L2_bis_CA"
	cp -a "${SHUNIT_TMPDIR}/PKI/CA" "${SHUNIT_TMPDIR}/PKI/L2_bis_CA"
	create_sub_ca "${SHUNIT_TMPDIR}/PKI/L2_bis_CA" "L2 certificate alt"
	sign_sub_ca "${SHUNIT_TMPDIR}/PKI/L1_CA" "${SHUNIT_TMPDIR}/PKI/L2_bis_CA"

	test_private_key="$(mktemp -p "${SHUNIT_TMPDIR}" "test_key.priv.pem.XXXXXX")"
	test_certificate_ROOT="$(mktemp -p "${SHUNIT_TMPDIR}" "test_root.crt.pem.XXXXXX")"
	test_certificate_L1="$(mktemp -p "${SHUNIT_TMPDIR}" "test_l1.crt.pem.XXXXXX")"
	test_certificate_L2="$(mktemp -p "${SHUNIT_TMPDIR}" "test_l2.crt.pem.XXXXXX")"
	test_certificate_leaf="$(mktemp -p "${SHUNIT_TMPDIR}" "test_leaf.crt.pem.XXXXXX")"
	test_certificate_chain="$(mktemp -p "${SHUNIT_TMPDIR}" "certificates_chain.XXXXXX")"
	test_public_key="$(mktemp -p "${SHUNIT_TMPDIR}" "test_key.pub.pem.XXXXXX")"
	test_public_key_der="$(mktemp -p "${SHUNIT_TMPDIR}" "test_key.pub.der.XXXXXX")"
	test_invalid_cert="$(mktemp -p "${SHUNIT_TMPDIR}" "test_cert.crt.pem.XXXXXX")"

	create_firmware_certificate "${SHUNIT_TMPDIR}/PKI/L2_CA" \
	                            "test_firmware_pki" \
	                            "${test_certificate_leaf}" \
	                            "${test_private_key}" \
	                            "${test_public_key}" \
	                            "${test_public_key_der}"

	cp "${SHUNIT_TMPDIR}/PKI/root_CA/pki/ca.crt" "${test_certificate_ROOT}"
	cp "${SHUNIT_TMPDIR}/PKI/L1_CA/pki/ca.crt" "${test_certificate_L1}"
	cp "${SHUNIT_TMPDIR}/PKI/L2_CA/pki/ca.crt" "${test_certificate_L2}"

	openssl req -newkey rsa:2048 \
	            -nodes \
	            -keyout key.pem \
	            -x509 \
	            -days 365 \
	            -out "${test_invalid_cert}" \
	            -subj "/C=NL/ST=State/L=City/O=Company Inc./OU=IT/CN=ESBS Updater C.I. testing root CA"

	cat \
	    "${test_certificate_leaf}" \
	    "${test_certificate_L2}" > "${test_certificate_chain}"


	append_payload_marker "${swupdate_src}"

	chain_offset="$(stat -c "%s" "${swupdate_src}")"
	append_metadata "${swupdate_src}" "${test_certificate_chain}"

	sign_files "${test_private_key}" "${swupdate_src}"
	append_metadata "${swupdate_src}" "${swupdate_src}.sig"

	payload_size="$(stat -c "%s" "${swupdate_src}")"
	signature_offset="$((payload_size + METADATA_PAYLOAD_MARKER_SIZE_BYTES))"
	signature_size="$(stat -c "%s" "${swupdate_src}.sig")"
	chain_size="$(stat -c "%s" "${test_certificate_chain}")"
	chain_size_offset="$((chain_offset + chain_size))"

	echo
	echo "================================================================================"
}

oneTimeTearDown()
{
	echo "Tearing down '${COMMAND_UNDER_TEST##*/}' test env"

	if [ -f "${swupdate_src}" ]; then
		unlink "${swupdate_src}"
	fi

	if [ -f "${swupdate_src}.sig" ]; then
		unlink "${swupdate_src}.sig"
	fi

	if [ -f "${test_private_key}" ]; then
		unlink "${test_private_key}"
	fi

	if [ -f "${test_certificate_ROOT}" ]; then
		unlink "${test_certificate_ROOT}"
	fi

	if [ -f "${test_certificate_L1}" ]; then
		unlink "${test_certificate_L1}"
	fi

	if [ -f "${test_certificate_L2}" ]; then
		unlink "${test_certificate_L2}"
	fi

	if [ -f "${test_certificate_leaf}" ]; then
		unlink "${test_certificate_leaf}"
	fi

	if [ -f "${test_certificate_chain}" ]; then
		unlink "${test_certificate_chain}"
	fi

	if [ -f "${test_public_key}" ]; then
		unlink "${test_public_key}"
	fi

	if [ -f "${test_public_key_der}" ]; then
		unlink "${test_public_key_der}"
	fi

	if [ -f "${test_invalid_cert}" ]; then
		unlink "${test_invalid_cert}"
	fi

	if [ -d "${SHUNIT_TMPDIR}/PKI/L1_CA/pki/" ]; then
		rm -rf "${SHUNIT_TMPDIR}/PKI/L1_CA/pki/"
	fi

	if [ -d "${SHUNIT_TMPDIR}/PKI/root_CA/pki/" ]; then
		rm -rf "${SHUNIT_TMPDIR}/PKI/root_CA/pki/"
	fi
}

setUp()
{
	echo

	test_swupdate_file="$(mktemp -p "${SHUNIT_TMPDIR}" "test_swupdate_file.XXXXXX")"
	cp "${swupdate_src}" "${test_swupdate_file}"
}

tearDown()
{
	unlink "${test_swupdate_file}"

	echo "--------------------------------------------------------------------------------"
}

testValidSWUpdateFile()
{
	_public_key_output="$(mktemp -p "${SHUNIT_TMPDIR}" "test_key.priv.pem.XXXXXX")"

	"${COMMAND_UNDER_TEST}" -r "${test_certificate_ROOT}" -p "${_public_key_output}" "${test_swupdate_file}"
	assertTrue "Correct update with chain of trust should be accepted." "[ ${?} -eq 0 ]"

	cmp "${test_public_key}" "${_public_key_output}"
	assertTrue "The public key should be correctly extracted." "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" -r "${test_certificate_ROOT}" "${test_swupdate_file}"
	assertTrue "Chain verification without extracting the public key should be accepted" "[ ${?} -eq 0 ]"

	unlink "${_public_key_output}"
}

testInsufficientParameters()
{
	"${COMMAND_UNDER_TEST}"
	assertFalse "Zero parameters is insufficient." "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" "${test_swupdate_file}"
	assertFalse "A root certificate need to be supplied." "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" -r "${test_certificate_ROOT}"
	assertFalse "A firmware update needs to be supplied." "[ ${?} -eq 0 ]"
}

testExcessiveParameters()
{
	"${COMMAND_UNDER_TEST}" "${test_swupdate_file}" "dummy1"
	assertFalse "Excessive parameter without key should not be accepted." "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" "${test_swupdate_file}" "dummy1" "dummy2"
	assertFalse "Excessive parameters without key should not be accepted." "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" -r "${test_certificate_ROOT}" "${test_swupdate_file}" "dummy1"
	assertFalse "Excessive parameter should not be accepted." "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" -r "${test_certificate_ROOT}"  "${test_swupdate_file}" "dummy1" "dummy2"
	assertFalse "Excessive parameters should not be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateZeroSignatureSize()
{
	echo "Zeroing update file chain size bytes."

	corrupt_file \
	             "${test_swupdate_file}" \
	             "${test_swupdate_file}" \
	             "${chain_size_offset}" \
	             "zero" \
	             "${METADATA_CHAIN_SIZE_BYTES}"

	"${COMMAND_UNDER_TEST}" -r "${test_certificate_ROOT}" "${test_swupdate_file}"
	assertFalse "Zero size chain should not be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateLargeSignatureSize()
{
	echo "Over-sizing update file chain size bytes."

	corrupt_bytes \
	              "${test_swupdate_file}" \
	              "$((signature_offset + signature_size))" \
	              "${METADATA_SIGNATURE_SIZE_BYTES}" \
	              "$((1024 * 1024))" \
	              "${METADATA_SIGNATURE_SIZE_MAX}"

	"${COMMAND_UNDER_TEST}" -r "${test_certificate_ROOT}" "${test_swupdate_file}"
	assertFalse "Over-sized signature should not be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateBadChain()
{
	echo "Corrupting update chain."

	corrupt_file \
	             "${test_swupdate_file}" \
	             "${test_swupdate_file}" \
	             "${chain_offset}" \
	             "urandom" \
	             "$((chain_size - 1))"

	"${COMMAND_UNDER_TEST}" -r "${test_certificate_ROOT}" "${test_swupdate_file}"
	assertFalse "Bad signature should not be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateInvalidRootCrt()
{
	"${COMMAND_UNDER_TEST}" -r "${test_invalid_cert}" "${test_swupdate_file}"
	assertFalse "Invalid Root cert should not be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateInvalidChainCrt()
{
	_test_invalid_chain="$(mktemp -p "${SHUNIT_TMPDIR}" "test_chain.cert.XXXXXX")"
	cat \
	    "${test_certificate_leaf}" \
	    "${SHUNIT_TMPDIR}/PKI/L2_bis_CA/pki/ca.crt"  > "${_test_invalid_chain}"

	prepare_swu "${test_swupdate_file}" "${test_private_key}" "${_test_invalid_chain}"

	"${COMMAND_UNDER_TEST}" -r "${test_certificate_ROOT}" "${test_swupdate_file}"
	assertFalse "Invalid chain should not be accepted." "[ ${?} -eq 0 ]"

	unlink "${_test_invalid_chain}"
}

testSWUpdateUnneededCrt()
{
	_longer_chain="$(mktemp -p "${SHUNIT_TMPDIR}" "test_chain.cert.XXXXXX")"
	cat "${test_certificate_chain}" "${test_invalid_cert}" > "${_longer_chain}"

	prepare_swu "${test_swupdate_file}" "${test_private_key}" "${_longer_chain}"

	"${COMMAND_UNDER_TEST}" -r "${test_certificate_ROOT}" "${test_swupdate_file}"
	assertTrue "Unneeded certs should not fail the verification." "[ ${?} -eq 0 ]"

	unlink "${_longer_chain}"
}

testSWUpdateUnneededCrl()
{
	_longer_chain="$(mktemp -p "${SHUNIT_TMPDIR}" "test_chain.cert.XXXXXX")"
	cat "${test_certificate_chain}" "${SHUNIT_TMPDIR}/PKI/root_CA/pki/crl.pem" > "${_longer_chain}"

	prepare_swu "${test_swupdate_file}" "${test_private_key}" "${_longer_chain}"

	"${COMMAND_UNDER_TEST}" -r "${test_certificate_ROOT}" "${test_swupdate_file}"
	assertTrue "Unneeded crl should not fail the verification." "[ ${?} -eq 0 ]"

	unlink "${_longer_chain}"
}

testExpiredCertificate()
{
	_public_key_output="$(mktemp -p "${SHUNIT_TMPDIR}" "test_key.priv.pem.XXXXXX")"

	_enddate="$(openssl x509 -enddate -noout -in "${test_certificate_leaf}")"
	echo "Faking time to '${_enddate#notAfter\=}'"
	faketime "${_enddate#notAfter\=}" \
	"${COMMAND_UNDER_TEST}" -r "${test_certificate_ROOT}" -p "${_public_key_output}" "${test_swupdate_file}"
	assertFalse "Expired certificate should be denied." "[ ${?} -eq 0 ]"

	unlink "${_public_key_output}"
}
