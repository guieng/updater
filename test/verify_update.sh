#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2018 Ultimaker B.V.
# Copyright (C) 2019 EVBox B.V.
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

set -eu

# Support any invocation of/with shunit2
if [ "${0}" = "${0%%shunit2}" ]; then
	"$(command -v shunit2)" "${0}"
	return "${?}"
fi
src_dir="${1%%${1##*/}}"
COMMAND_UNDER_TEST="${COMMAND_UNDER_TEST:-${src_dir}/../scripts/${1##*/}}"
shift

FIXTURES="${FIXTURES:-${src_dir}/fixtures/}"

# shellcheck source=test/common.inc.sh
. "${src_dir}/common.inc.sh"

DEF_SSL_PUBLIC_KEY="/etc/system_update/firmware_update_key.pem"

set +eu


create_firmware_certificate()
{
	_pki_dir="${1?}"
	_certificate="${2?}"
	_test_private_key="${3?}"
	_test_certificate="${4?}"
	_test_public_key="${5?}"
	_test_public_key_der="${6:-}"

	(
		cd "${_pki_dir}"

		EASYRSA_REQ_CN="ESBS Updater C.I. testing software update signing certificate" \
		./easyrsa --batch gen-req "${_certificate}" nopass

		./easyrsa --batch sign-req code-signing "${_certificate}"

		./easyrsa "show-cert" "${_certificate}"
	)

	if [ ! -f "${_pki_dir}/pki/private/${_certificate}.key" ]; then
		fail "Failed to create private test key"
	fi
	cp "${_pki_dir}/pki/private/${_certificate}.key" "${_test_private_key}"

	if [ ! -f "${_pki_dir}/pki/issued/${_certificate}.crt" ]; then
		fail "Failed to create test certificate"
	fi
	cp "${_pki_dir}/pki/issued/${_certificate}.crt" "${_test_certificate}"

	openssl x509 \
	        -in "${_test_certificate}" \
	        -out "${_test_public_key}" \
		-outform "PEM" \
	        -noout \
	        -pubkey

	if [ ! -f "${_test_public_key}" ]; then
		fail "Failed to create public PEM test key"
	fi

	if [ -n "${_test_public_key_der:-}" ]; then
		openssl rsa \
		        -pubin \
		        -in "${_test_public_key}" \
		        -outform "DER" \
		        -out "${_test_public_key_der}"

		if [ ! -f "${_test_public_key_der}" ]; then
			fail "Failed to create public DER test key"
		fi
	fi
}

sign_sub_ca()
{
	_pki_root_dir="${1?}"
	_pki_swu_dir="${2?}"
	(
		cd "${_pki_root_dir}"
		./easyrsa --batch import-req "${_pki_swu_dir}/pki/reqs/ca.req" "swu_ca"
		./easyrsa --batch sign-req ca "swu_ca"
	)
	cat "${_pki_root_dir}/pki/issued/swu_ca.crt" "${_pki_root_dir}/pki/ca.crt" > "${_pki_swu_dir}/pki/ca.crt"
	(
		cd "${_pki_swu_dir}"
		./easyrsa show-ca
		./easyrsa gen-crl
	)
}

create_sub_ca()
{
	_pki_dir="${1?}"
	_cn_suffix="${2?}"

	rm -rf "${_pki_dir}/pki"
	(
		cd "${_pki_dir}"
		./easyrsa init-pki
		EASYRSA_REQ_CN="ESBS Updater C.I. ${_cn_suffix}" ./easyrsa --batch build-ca nopass subca
	)
}

create_root_ca()
{
	_pki_dir="${1?}"
	rm -rf "${_pki_dir}/pki"
	(
		cd "${_pki_dir}"
		./easyrsa init-pki
		EASYRSA_REQ_CN="ESBS Updater C.I. testing root CA" ./easyrsa --batch build-ca nopass
		./easyrsa gen-crl
		./easyrsa show-ca
	)
}

oneTimeSetUp()
{
	echo "Setting up '${COMMAND_UNDER_TEST##*/}' test env"

	swupdate_src="$(mktemp -p "${SHUNIT_TMPDIR}" "swupdate_src.XXXXXX")"
	cp "${SOFTWARE_UPDATE_FILE}" "${swupdate_src}"

	echo "Generating PKI"
	cp -a "${FIXTURES}/PKI" "${SHUNIT_TMPDIR}"

	echo "Generating Root_CA"
	cp -a "${SHUNIT_TMPDIR}/PKI/CA" "${SHUNIT_TMPDIR}/PKI/root_CA"
	create_root_ca "${SHUNIT_TMPDIR}/PKI/root_CA"

	echo "Generating swu_CA"
	cp -a "${SHUNIT_TMPDIR}/PKI/CA" "${SHUNIT_TMPDIR}/PKI/swu_CA"
	create_sub_ca "${SHUNIT_TMPDIR}/PKI/swu_CA" "testing software update CA"
	sign_sub_ca "${SHUNIT_TMPDIR}/PKI/root_CA" "${SHUNIT_TMPDIR}/PKI/swu_CA"

	test_private_key="$(mktemp -p "${SHUNIT_TMPDIR}" "test_key.priv.pem.XXXXXX")"
	test_certificate="$(mktemp -p "${SHUNIT_TMPDIR}" "test_key.crt.pem.XXXXXX")"
	test_public_key="$(mktemp -p "${SHUNIT_TMPDIR}" "test_key.pub.pem.XXXXXX")"
	test_public_key_der="$(mktemp -p "${SHUNIT_TMPDIR}" "test_key.pub.der.XXXXXX")"

	create_firmware_certificate "${SHUNIT_TMPDIR}/PKI/swu_CA" \
	                            "test_firmware_pki" \
	                            "${test_private_key}" \
	                            "${test_certificate}" \
	                            "${test_public_key}" \
	                            "${test_public_key_der}"
	append_payload_marker "${swupdate_src}"
	sign_files "${test_private_key}" "${swupdate_src}"
	append_metadata "${swupdate_src}" "${swupdate_src}.sig"

	payload_size="$(stat -c "%s" "${swupdate_src}")"
	signature_offset="$((payload_size + METADATA_PAYLOAD_MARKER_SIZE_BYTES))"
	signature_size="$(stat -c "%s" "${swupdate_src}.sig")"
	test_public_key_size="$(stat -c "%s" "${test_public_key}")"

	echo
	echo "================================================================================"
}

oneTimeTearDown()
{
	echo "Tearing down '${COMMAND_UNDER_TEST##*/}' test env"

	if [ -f "${swupdate_src}" ]; then
		unlink "${swupdate_src}"
	fi

	if [ -f "${swupdate_src}.sig" ]; then
		unlink "${swupdate_src}.sig"
	fi

	if [ -f "${swupdate_src}.swu" ]; then
		unlink "${swupdate_src}.swu"
	fi

	if [ -f "${test_public_key}" ]; then
		unlink "${test_public_key}"
	fi

	if [ -f "${test_public_key_der:-}" ]; then
		unlink "${test_public_key_der}"
	fi

	if [ -d "${SHUNIT_TMPDIR}/PKI/swu_CA/pki/" ]; then
		rm -rf "${SHUNIT_TMPDIR}/PKI/swu_CA/pki/"
	fi

	if [ -d "${SHUNIT_TMPDIR}/PKI/root_CA/pki/" ]; then
		rm -rf "${SHUNIT_TMPDIR}/PKI/root_CA/pki/"
	fi
}

setUp()
{
	echo

	test_swupdate_file="$(mktemp -p "${SHUNIT_TMPDIR}" "test_swupdate_file.XXXXXX")"
	cp "${swupdate_src}" "${test_swupdate_file}"
}

tearDown()
{
	unlink "${test_swupdate_file}"

	echo "--------------------------------------------------------------------------------"
}

testValidSWUpdateFile()
{
	"${COMMAND_UNDER_TEST}" -p "${test_public_key}" "${test_swupdate_file}"
	assertTrue "Correct update should be accepted." "[ ${?} -eq 0 ]"

	# For testing this key needs to be on the (docker)host installed.
	install -D -m 644 -t "${test_public_key}" "${DEF_SSL_PUBLIC_KEY}"

	"${COMMAND_UNDER_TEST}" "${test_swupdate_file}"
	assertTrue "Update without certificate should not be accepted." "[ ${?} -ne 0 ]"

	if [ -f "${DEF_SSL_PUBLIC_KEY}" ]; then
		unlink "${DEF_SSL_PUBLIC_KEY}"
	fi

	# Only one level is being cleaned, note this during path changes.
	if [ -d "$(dirname "${DEF_SSL_PUBLIC_KEY}")" ]; then
		rmdir "$(dirname "${DEF_SSL_PUBLIC_KEY}")"
	fi
	assertTrue "Correct update with PEM key should be accepted." "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" -p "${test_public_key_der}" "${test_swupdate_file}"
	assertTrue "Correct update with DER key should be accepted." "[ ${?} -eq 0 ]"
}

testInsufficientParameters()
{
	"${COMMAND_UNDER_TEST}"
	assertFalse "Zero parameters is insufficient." "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" "${test_swupdate_file}"
	assertTrue "Update without valid certificate should not be accepted." "[ ${?} -ne 0 ]"

	"${COMMAND_UNDER_TEST}" -p "${test_public_key}"
	assertFalse "A firmware update needs to be supplied." "[ ${?} -eq 0 ]"
}

testExcessiveParameters()
{
	"${COMMAND_UNDER_TEST}" "${test_swupdate_file}" "dummy1"
	assertFalse "Excessive parameter without key should not be accepted." "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" "${test_swupdate_file}" "dummy1" "dummy2"
	assertFalse "Excessive parameters without key should not be accepted." "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" -p "${test_public_key}" "${test_swupdate_file}" "dummy1"
	assertFalse "Excessive parameter should not be accepted." "[ ${?} -eq 0 ]"

	"${COMMAND_UNDER_TEST}" -p "${test_public_key}" "${test_swupdate_file}" "dummy1" "dummy2"
	assertFalse "Excessive parameters should not be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateZeroSignatureSize()
{
	echo "Zeroing update file signature size bytes."

	corrupt_file \
	             "${test_swupdate_file}" \
	             "${test_swupdate_file}" \
	             "$((signature_offset + signature_size))" \
	             "zero" \
	             "${METADATA_SIGNATURE_SIZE_BYTES}"

	"${COMMAND_UNDER_TEST}" -p "${test_public_key}" "${test_swupdate_file}"
	assertFalse "Zero size signature should not be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateLargeSignatureSize()
{
	echo "Over-sizing update file signature size bytes."

	corrupt_bytes \
	              "${test_swupdate_file}" \
	              "$((signature_offset + signature_size))" \
	              "${METADATA_SIGNATURE_SIZE_BYTES}" \
	              "$((1024 * 1024))" \
	              "${METADATA_SIGNATURE_SIZE_MAX}"

	"${COMMAND_UNDER_TEST}" -p "${test_public_key}" "${test_swupdate_file}"
	assertFalse "Over-sized signature should not be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateRandomSignatureSize()
{
	echo "Randomizing update file signature size bytes."

	corrupt_bytes \
	              "${test_swupdate_file}" \
	              "$((signature_offset + signature_size))" \
	              "${METADATA_SIGNATURE_SIZE_BYTES}" \
	              "${signature_size}"

	"${COMMAND_UNDER_TEST}" -p "${test_public_key}" "${test_swupdate_file}"
	assertFalse "Invalid signature size should not be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateBadSignature()
{
	echo "Corrupting update file signature."

	corrupt_file \
	             "${test_swupdate_file}" \
	             "${test_swupdate_file}" \
	             "$(random_int "${signature_offset}" \
	                           "$((signature_offset + signature_size - 1))")" \
	             "urandom" \
	             1

	"${COMMAND_UNDER_TEST}" -p "${test_public_key}" "${test_swupdate_file}"
	assertFalse "Bad signature should not be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateCorruptPayload()
{
	echo "Corrupting update file payload"

	corrupt_file \
	             "${test_swupdate_file}" \
	             "${test_swupdate_file}" \
	             "$(random_int "$((payload_size - 1))")" \
	             "urandom" \
	             1

	"${COMMAND_UNDER_TEST}" -p "${test_public_key}" "${test_swupdate_file}"
	assertFalse "Corrupted payload should not be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateAlternativePublicKey()
{
	echo "Generating new public key from same private key"

	openssl pkey \
	             -in "${test_private_key}" \
	             -out "${SHUNIT_TMPDIR}/alternative_key.pub.pem" \
	             -pubout

	"${COMMAND_UNDER_TEST}" -p "${SHUNIT_TMPDIR}/alternative_key.pub.pem" "${test_swupdate_file}"
	assertTrue "Alternative public key should be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateInvalidKeyPair()
{
	echo "Generating new key pair"

	_test_invalid_private_key="$(mktemp -p "${SHUNIT_TMPDIR}" "test_key.priv.pem.XXXXXX")"
	_test_invalid_public_key="$(mktemp -p "${SHUNIT_TMPDIR}" "test_key.pub.pem.XXXXXX")"

	create_simple_keypair \
	                      "${_test_invalid_private_key}" \
	                      "${_test_invalid_public_key}"

	"${COMMAND_UNDER_TEST}" -p "${_test_invalid_public_key}" "${test_swupdate_file}"
	assertFalse "Invalid key pair should not be accepted." "[ ${?} -eq 0 ]"
}

testSWUpdateCorruptPublicKey()
{
	echo "Corrupting public key"

	corrupt_file \
	             "${test_public_key}" \
	             "${SHUNIT_TMPDIR}/corrupt_key.pub.pem" \
	             "$(random_int "$((test_public_key_size - 1))")" \
	             "urandom" \
	             1

	"${COMMAND_UNDER_TEST}" -p "${SHUNIT_TMPDIR}/corrupt_key.pub.pem" "${test_swupdate_file}"
	assertFalse "Corrupted public key should not be accepted." "[ ${?} -eq 0 ]"
}
